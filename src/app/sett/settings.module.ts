import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from "@ngx-translate/core";
import { ComponentsModule } from '../components/components.module';

import { SettingsPage } from './settings.page';
import { FormsModule } from '@angular/forms';
import { NgxAutocomPlaceModule } from 'ngx-autocom-place';

const settingsRoutes: Routes = [
  {
    path: '',
    component: SettingsPage
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(settingsRoutes),
    ComponentsModule,
    NgxAutocomPlaceModule,
    TranslateModule
  ],
  declarations: [ SettingsPage ]
})
export class SettingsPageModule {}
