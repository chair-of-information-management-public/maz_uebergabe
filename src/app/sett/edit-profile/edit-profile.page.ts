import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { ModalController, ToastController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { UserInfoService } from "../../services/userInfo.service";
import { Storage } from "@ionic/storage";
@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"],
})
export class EditProfilePage implements OnInit {
  constructor(
    public modalController: ModalController,
    private userInfoService: UserInfoService,
    private translate: TranslateService,
    private toastController: ToastController,
    private storage: Storage,
    private ref: ChangeDetectorRef
  ) {}
  @Input() editOption: string;
  @Input() data: string;
  oldUserName;
  uid;
  selectedPlace: any;
  ngOnInit() {
    this.userInfoService.getCurrentUser().then((uid) => {
      this.uid = uid;
    });
    console.log(this.editOption, this.data);
    if (this.editOption == "username") this.oldUserName = this.data;
  }
  dismissModal() {
    this.modalController.dismiss({
      dismissed: true,
    });
  }
  changeUserName() {
    if (this.data.length == 0) this.data = this.oldUserName;
    this.userInfoService.setUsername(this.uid, this.data);
  }
  changeHomeAdress() {

    if (this.data.length <= 0) {
      this.storage.remove("adress");
    } else {
      this.storage.set("adress", this.data);
    }
  }
  placeChangedHome(place) {
    this.selectedPlace = place;
    this.ref.detectChanges();
    this.data = this.selectedPlace.formatted_address;
  }
  save() {
    if (this.editOption == "username") this.changeUserName();
    else if (this.editOption == "home") this.changeHomeAdress();
    this.dismissModal();
    this.presentToast();
  }

  async presentToast() {
    const Einstellungen = this.translate.instant("Einstellungen");

    const toast = await this.toastController.create({
      message: Einstellungen,
      duration: 3000,
    });
    toast.present();
  }
}
