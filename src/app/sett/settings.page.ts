import { ChangeDetectorRef, Component, NgZone, OnDestroy } from "@angular/core";
import { Location } from "@angular/common";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import {
  AlertController,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { Subscription } from "rxjs";

import { HistoryHelperService } from "../utils/history-helper.service";
import { LanguageService } from "../language/language.service";
import { TranslateService } from "@ngx-translate/core";
import { AlertService } from "../services/alert.service";
import { Storage } from "@ionic/storage";
import { FirebaseAuthService } from "../firebase/auth/firebase-auth.service";
import { MenuController } from "@ionic/angular";
import { AfterViewInit, ElementRef, ViewChild } from "@angular/core";
import { Environment } from "../services/environment.service";
import { url } from "inspector";
import { UserInfoService } from "../services/userInfo.service";
import { AngularFireStorage} from "@angular/fire/storage";
import { finalize } from "rxjs/operators";
import { AngularFirestore } from "@angular/fire/firestore";
import waitUntil from "async-wait-until";
import { skip } from "rxjs/operators";
import { ModalController } from "@ionic/angular";
import { EditProfilePage } from "./edit-profile/edit-profile.page";

@Component({
  selector: "app-categories",
  templateUrl: "./settings.page.html",
  styleUrls: [
    "./styles/settings.page.scss",
    "./styles/settings.shell.scss",
    "./styles/settings.responsive.scss",
  ],
})
export class SettingsPage {
  //@ViewChild("google-maps-script") oldScript: ElementRef;
  uid: string;
  username: string;
  avatar: any;
  adress: string;
  loginForm: FormGroup;
  subscriptions: Subscription;
  submitError: string;
  redirectLoader: HTMLIonLoadingElement;
  authRedirectResult: Subscription;
  available_languages = [];
  translations;
  validation_messages;
  translateCheck;
  currentAdress;
  selectedPlace: any;
  flag;
  name;
  obj;
  gps;
  files = [];
  uploadProgress = 0;
  oldUserName: string;
  constructor(
    public router: Router,
    public languageService: LanguageService,
    public route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private alertService: AlertService,
    public loadingController: LoadingController,
    public location: Location,
    public historyHelper: HistoryHelperService,
    public translate: TranslateService,
    private auth: FirebaseAuthService,
    public alertController: AlertController,
    private storage: Storage,
    public menuCtrl: MenuController,
    public toastController: ToastController,
    private environmentService: Environment,
    private userInfoService: UserInfoService,
    private angularFireStorage: AngularFireStorage,
    private afs: AngularFirestore,
    public modalController: ModalController
  ) {}
  ngOnInit(): void {
    this.obj = this.languageService.getFlags(this.translate.currentLang);
    this.flag = this.obj.flag;
    this.name = this.flag.name;
    this.storage.get("adress").then((value: any) => {
      this.adress = value;
    });
    this.subscriptions = this.route.data.subscribe(
      (state) => {
        this.getTranslations();
      },
      (error) => console.log(error)
    );

    this.translate.onLangChange.subscribe(() => this.getTranslations());
    this.translate.get;

    this.userInfoService.getCurrentUser().then((uid) => {
      this.uid = uid;
      this.getUserInfo();
      this.oldUserName = this.username;
    });
  }
  ionViewWillEnter() {
    this.getUserInfo();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getUserInfo();
      this.storage.get("adress").then((value: any) => {
        this.adress = value;
      });
      event.target.complete();
    }, 2000);
  }
  getUserInfo() {
    this.userInfoService.getUserInfoFromId(this.uid).then((info) => {
      this.username = info.username;
      this.avatar = info.photoURL;
    });
  }
  async deleteImage(){
    // Storage path
    const fileStoragePath = `Avatare/${this.uid}`;
    // Image reference
    const imageRef = this.angularFireStorage.ref(fileStoragePath);
    imageRef.delete();
    // Set default avatar as avatar
    this.afs.doc(`normalUser/${this.uid}`).update({
      avatar : "../../../../assets/icon/avatar_default.png"
    })
    const toast = await this.toastController.create({
      message:
        "Das Profilbild wurde geändert. Scrollen Sie nach unten, um das neue Bild zu aktualisieren",
      duration: 3000,
    });
    toast.present();
  }
  uploadImage(data: FileList) {
    const file = data[0];
    // Image validation
    if (file.type.split("/")[0] !== "image") {
      console.log("File type is not supported!");
      return;
    }
    // Storage path
    const fileStoragePath = `Avatare/${this.uid}`;
    // Image reference
    const imageRef = this.angularFireStorage.ref(fileStoragePath);
    // File upload task
    var fileUploadTask = this.angularFireStorage.upload(fileStoragePath, file);
    // Show uploading progress
    fileUploadTask.then(async () => {

      var UploadedImageURL = imageRef.getDownloadURL();

      UploadedImageURL.subscribe(
        (resp) => {
          console.log(resp);

          this.storeFilesFirebase(resp);
        },
        (error) => {
          console.log(error);
        }
      );
      const toast = await this.toastController.create({
        message:
          "Das Profilbild wurde geändert. Scrollen Sie nach unten, um das neue Bild zu aktualisieren",
        duration: 3000,
      });
      toast.present();
    });
  }
  storeFilesFirebase(imgPath) {
    this.userInfoService.getUserRole().then((role) => {
      if (role == "NORMALUSER") {
        this.afs
          .collection("normalUser")
          .doc(this.uid)
          .update({ avatar: imgPath });
      } else if (role == "INITIATIVE")
        this.afs.collection("pages").doc(this.uid).update({ icon: imgPath });
    });
  }

  changeHomeAdress() {
    if (this.adress.length <= 0) {
      this.storage.remove("adress");
    } else {
      this.storage.set("adress", this.adress);
    }
  }
  async presentToast() {
    const Einstellungen = this.translate.instant("Einstellungen");

    const toast = await this.toastController.create({
      message: Einstellungen,
      duration: 3000,
    });
    toast.present();
  }

  async deleteAccount(){
    var alertHeader = this.translate.instant("AccountDeleteHeader")
    var alertText = this.translate.instant("AccountDeleteConfirm")
    const alert = await this.alertController.create({
      header: alertHeader,
      message: alertText, //translate here
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: "Okay",
          handler: () => {
            var currentUser = this.auth.currentUser
            this.alertService.presentAlertGoTo
            this.auth.signOut().subscribe(
              () => {
                // Sign-out successful.
                // Replace state as we are no longer authorized to access profile page.
                this.router.navigate(["firebase/auth/sign-in"], { replaceUrl: true })
                this.auth.deleteUser(currentUser)
                var DeletionHeader = this.translate.instant("AccountDeletion")
                var DeletionText   = this.translate.instant("AccountDeleteText")
                var ButtonText     = this.translate.instant("OK")
                this.alertService.presentAlertFavorite(DeletionHeader, DeletionText, ButtonText)
              },
              (error) => {
                console.log("signout error", error);
              }
            );
          },
        },
      ],
    });

    await alert.present();
   
  }
  changeUserName() {
    if (this.username.length == 0) this.username = this.oldUserName;
    this.userInfoService.setUsername(this.uid, this.username);
  }
  placeChangedHome(place) {
    this.selectedPlace = place;
    this.ref.detectChanges();
    this.adress = this.selectedPlace.formatted_address;
  }
  async openLanguageChooser() {
    this.available_languages = this.languageService
      .getLanguages()
      .map((item) => ({
        name: item.name,
        type: "radio",
        label: item.name,
        value: item.code,
        flag: item.flag,
        checked: item.code === this.translate.currentLang,
      }));

    const alert = await this.alertController.create({
      header: this.translations.SELECT_LANGUAGE,
      inputs: this.available_languages,
      cssClass: "language-alert",
      buttons: [
        {
          text: this.translations.CANCEL,
          role: "cancel",
          cssClass: "secondary",
          handler: () => {},
        },
        {
          text: this.translations.OK,
          handler: async (data) => {
            if (data) {
              await this.translate.use(data);
              await this.storage.set("language", data);
              this.translate.currentLang = await data;
              //this.changeGoogleMapsLanguage(data);
              this.presentToast();
            }
            this.obj = await this.languageService.getFlags(
              this.translate.currentLang
            );
            this.flag = this.obj.flag;
            this.name = this.flag.name;
            window.location.reload(); //reload app after language change
          },
        },
      ],
    });

    await alert.present();

    var radios = document.getElementsByClassName("alert-radio-label");
    for (let index = 0; index < radios.length; index++) {
      let singrad = radios[index];
      singrad.innerHTML = singrad.innerHTML.concat(
        "<img src=" +
          this.available_languages[index].flag +
          ' style="width:30px; position:absolute; right:10px;"/>'
      );
    }
  }

  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(this.translate.currentLang)
      .subscribe((translations) => (this.translations = translations));
  }
  goBack() {
    this.location.back();
  }
  async presentModal(editOption) {
    let data;
    switch (editOption) {
      case "username":
        data = this.username;
        break;
      case "home":
        data = this.adress;
        break;
    }
    const modal = await this.modalController.create({
      component: EditProfilePage,
      componentProps: {
        editOption: editOption,
        data: data,
      },
    });
    modal.onDidDismiss().then((data) => {
      this.getUserInfo();
      this.storage.get("adress").then((value: any) => {
        this.adress = value;
      });
    });
    return await modal.present();
  }
}
