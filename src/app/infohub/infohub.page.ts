import { Component } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { LanguageService } from "../language/language.service";
@Component({
  selector: "app-infohub",
  templateUrl: "./infohub.page.html",
  styleUrls: [
    "./styles/categories.page.scss",
    "./styles/categories.shell.scss",
    "./styles/categories.responsive.scss",
  ],
})
export class InfohubPage {
  pages = [];
  data: any;
  constructor(
    private db: AngularFirestore,
    public languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    //move this to categories
    this.db
      .collection("infohubpages")
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          // doc.data() is never undefined for query doc snapshots
          let data = this.data;
          data = doc.data();
          this.pages.push({
            id: doc.id,
            icon: data.icon,
            link: data.route,
          });
        });
      });
  }
}
