import { IonicModule } from "@ionic/angular";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsModule } from "../../components/components.module";

import { CategoriesPage } from "./categories.page";
import { TranslateModule } from "@ngx-translate/core";
import { MyCommonModule } from "../../my-common/my-common.module";

const categoriesRoutes: Routes = [
  {
    path: "",
    component: CategoriesPage,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild(categoriesRoutes),
    ComponentsModule,
    TranslateModule,
    MyCommonModule,
  ],
  declarations: [CategoriesPage],
})
export class CategoriesPageModule {}
