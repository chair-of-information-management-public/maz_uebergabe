import { Component } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { combineLatest, Subscription } from "rxjs";
import { LanguageService } from "../../language/language.service";
import { first } from "rxjs/operators";
import { LogoService } from "../../services/logo.service";
import { BulletinBoardService } from "../../services/bulletin-board.service";
import { UserInfoService } from "../../services/userInfo.service";
@Component({
  selector: "app-categories",
  templateUrl: "./categories.page.html",
  styleUrls: [
    "./styles/categories.page.scss",
    "./styles/categories.shell.scss",
    "./styles/categories.responsive.scss",
  ],
})
export class CategoriesPage {
  pages = [];
  tmp: any;
  data: any;
  searchIntegration: false;
  searchBehoerde: false;
  searchFreetime: false;
  searchterm = "";
  integration: string;
  mobility: string;
  freetime: string;
  searchbar;
  behoerde: string;
  integrationColor: string;
  mobilityColor: string;
  freetimeColor: string;
  behoerdeColor: string;
  searchMobility: false;
  contentLoaded = false;
  hideMe = true;
  translations;
  subscriptions: Subscription;
  constructor(
    private db: AngularFirestore,
    public languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router,
    private logoService: LogoService,
    private translate: TranslateService,
    private bulletinBoardService: BulletinBoardService,
    private userInfoSrvice: UserInfoService
  ) {}
  /**
   * load all pages
   */
  async initializeItems() {
    await this.db
      .collection("pages")
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          doc.data();
          let data = this.data;
          data = doc.data();
          this.pages.push({
            id: doc.id,
            name: data["name_" + this.translate.currentLang],
            standort: data.address.split(","),
            icon: data.icon,
            keywords: data.keywords,
            address: data.address,
            freizeit: data.freizeit,
            behoerde: data.behoerde,
            mobilitaet: data.mobilitaet,
            integration: data.integration,
          });
          this.contentLoaded = true;
        });
      });
    this.tmp = this.pages;
    this.integration = this.logoService.getIntegrationLogo();
    this.behoerde = this.logoService.getBehoerdeLogo();
    this.mobility = this.logoService.getMobilityLogo();
    this.freetime = this.logoService.getFreetimeLogo();
    this.integrationColor = this.logoService.getIntegrationColor();
    this.behoerdeColor = this.logoService.getBehoerdeColor();
    this.mobilityColor = this.logoService.getMobilityColor();
    this.freetimeColor = this.logoService.getFreetimeColor();
  }
  async ngOnInit() {
    this.initializeItems();
  }
  /**
   * filter pages based on the chosen icon
   */
  handleInputIcon() {
    this.searchterm = "";
    if (this.searchIntegration) {
      this.searchterm = "integration";
    }
    if (this.searchBehoerde) {
      this.searchterm = this.searchterm + "" + "behoerde";
    }
    if (this.searchMobility) {
      this.searchterm = this.searchterm + "" + "mobilitaet";
    }
    if (this.searchFreetime) {
      this.searchterm = this.searchterm + "" + "freizeit";
    }

    this.handleInput("", false);
  }
  /**
   * filter pages based on input search term
   * @param event
   * @param newSearchterm
   */
  handleInput(event, newSearchterm: boolean) {
    this.pages = this.tmp;
    if (newSearchterm) {
      this.searchterm = event.target.value.toLowerCase();
    }

    if (!this.searchterm) {
      this.searchterm = " ";
    } //if searchterm is empty then no filter needed
    this.pages = this.pages.filter((currentPage) => {
      // only search possible POIs in behoerde array
      if (
        currentPage &&
        this.searchterm &&
        this.searchBehoerde &&
        currentPage.behoerde == "behoerde"
      ) {
        return (
          currentPage.name?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.address?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.freizeit?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.mobilitaet?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.integration?.toLowerCase().indexOf(this.searchterm) >
            -1 ||
          currentPage.behoerde?.toLowerCase().indexOf(this.searchterm) > -1
        );
      }
      // only search possible POIs in freetime array
      if (
        currentPage &&
        this.searchterm &&
        this.searchFreetime &&
        currentPage.freizeit == "freizeit"
      ) {
        return (
          currentPage.name?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.address?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.freizeit?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.mobilitaet?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.integration?.toLowerCase().indexOf(this.searchterm) >
            -1 ||
          currentPage.behoerde?.toLowerCase().indexOf(this.searchterm) > -1
        );
      }
      // only search possible POIs in integration array
      if (
        currentPage &&
        this.searchterm &&
        this.searchIntegration &&
        currentPage.integration == "integration"
      ) {
        return (
          currentPage.name?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.address?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.freizeit?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.mobilitaet?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.integration?.toLowerCase().indexOf(this.searchterm) >
            -1 ||
          currentPage.behoerde?.toLowerCase().indexOf(this.searchterm) > -1
        );
      }
      // only search possible POIs in mobilitaet array
      if (
        currentPage &&
        this.searchterm &&
        this.searchMobility &&
        currentPage.mobilitaet == "mobilitaet"
      ) {
        return (
          currentPage.name?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.address?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.freizeit?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.mobilitaet?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.integration?.toLowerCase().indexOf(this.searchterm) >
            -1 ||
          currentPage.behoerde?.toLowerCase().indexOf(this.searchterm) > -1
        );
      }
      if (
        currentPage &&
        this.searchterm &&
        !this.searchMobility &&
        !this.searchFreetime &&
        !this.searchIntegration &&
        !this.searchBehoerde
      ) {
        return (
          currentPage.name?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.address?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.freizeit?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.mobilitaet?.toLowerCase().indexOf(this.searchterm) > -1 ||
          currentPage.integration?.toLowerCase().indexOf(this.searchterm) >
            -1 ||
          currentPage.behoerde?.toLowerCase().indexOf(this.searchterm) > -1
        );
      }
    });
  }
  /**
   * send card data to map
   * @param card
   */
  sendCardData(card: boolean) {
    this.hideMe = !this.hideMe;
    this.router.navigate(["app/maps/maps"], { state: { card: card } });
  }
}
