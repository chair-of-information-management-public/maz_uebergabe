import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireStorage } from "@angular/fire/storage";
import { Subscription } from "rxjs";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { url } from "node:inspector";
import { Router } from "@angular/router";
import { ElementRef, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Environment } from "../../../services/environment.service";
import { LogoService } from"../../../services/logo.service";

@Component({
  selector: "app-info",
  templateUrl: "./info.page.html",
  styleUrls: ["./info.page.scss"],
})
export class InfoPage implements OnInit {
  @ViewChild("descriptionContent") elementView: ElementRef;
  slug: string;
  page: any;
  content: string;
  imageURL: any;
  name: string;
  address: string;
  oeffnungszeiten: string;
  descriptions: [];
  activities: [];
  link: any;
  facebook: any;
  tickets: any;
  subs: Subscription;
  email: any;
  phoneNumber: any;
  integrationColor: string;
  mobilityColor: string;
  freetimeColor: string;
  behoerde: any;
  freetime: any;
  behoerdeColor: string;
  displayMail = true;
  displayPhone = true;
  displayFacebook = true;
  displayMobilitaet = false;
  displayFreizeit = false;
  displayBehoerde = false;
  displayIntegration = false;
  public items: any = [];
  contentHeight = "200px";

  constructor(
    private router: Router,
    public translate: TranslateService,
    private db: AngularFirestore,
    private route: ActivatedRoute,
    private logoService: LogoService,
    private storage: AngularFireStorage,
    public environmentService: Environment
  ) {
    this.items = [{ expanded: false }];
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.loadPage(params.get("slug") || "wiki");
    });
    this.integrationColor = this.logoService.getIntegrationColor();
    this.behoerdeColor = this.logoService.getBehoerdeColor();
    this.mobilityColor = this.logoService.getMobilityColor();
    this.freetimeColor = this.logoService.getFreetimeColor();
    this.behoerde = this.logoService.getBehoerdeLogo();
    this.freetime = this.logoService.getFreetimeLogo();
  }

  loadPage(slug) {
    if (this.subs) {
      this.subs.unsubscribe();
    }

    const doc = this.db.collection("pages").doc(slug).get();

    //read all documents from pages
    // this.db
    //   .collection("pages")
    //   .get()
    //   .subscribe((querySnapshot) => {
    //     querySnapshot.forEach((doc) => {
    //       // doc.data() is never undefined for query doc snapshots
    //     });
    //   });


    this.subs = doc.subscribe((snapshot) => {
      this.page = snapshot.data();
      if (!this.page) {
        //this.content = "### This page does not exist";
        this.slug = undefined;
      } else {
        this.slug = slug;
        //this.content = this.page.content;
        // get name, descriptions, and activities for chosen language
        // dynamic language through language choser
        this.name = this.page["name_" + this.translate.currentLang];
        this.descriptions =
          this.page["description_" + this.translate.currentLang];
        this.activities = this.page["activities_" + this.translate.currentLang];

        // get POI adress and details
        this.address = this.page.address;
        this.oeffnungszeiten = this.page["oeffnungszeiten_" + this.translate.currentLang]
        this.link = this.page.link;
        this.facebook = this.page.facebook;
        this.imageURL = this.page.imageURL;

        //check if poi has mail, phone or facebook
        if (!this.page.email) this.displayMail = false;
        this.email = "mailto:" + this.page.email;
        if (!this.page.phoneNumber) this.displayPhone = false;
        if (!this.page.facebook) this.displayFacebook = false;
        if(this.page.mobilitaet) this.displayMobilitaet = true;
        if(this.page.freizeit) this.displayFreizeit = true;
        if(this.page.behoerde) this.displayBehoerde = true;
        if(this.page.integration) this.displayIntegration = true;
        this.phoneNumber = "tel:" + this.page.phoneNumber;
      }
    });
  }
  sendData(adress: any, bool: boolean) {
    this.router.navigate(["app/maps/maps"], {
      state: { adress: adress, bool: bool },
    });
  }
  expandItem(item): void {
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map((listItem) => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }
}
