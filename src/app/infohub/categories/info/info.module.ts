import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { InfoPageRoutingModule } from "./info-routing.module";

import { InfoPage } from "./info.page";
import { TranslateModule } from "@ngx-translate/core";
import { MyCommonModule } from "../../../my-common/my-common.module";
import { ComponentsModule } from "../../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoPageRoutingModule,
    TranslateModule,
    MyCommonModule,
    ComponentsModule,
  ],
  declarations: [InfoPage],
})
export class InfoPageModule {}
