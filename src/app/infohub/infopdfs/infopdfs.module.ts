import { IonicModule } from "@ionic/angular";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsModule } from "../../components/components.module";

import { InfopdfsPage } from "./infopdfs.page";
import { TranslateModule } from "@ngx-translate/core";
import { MyCommonModule } from "../../my-common/my-common.module";

const infopdfsRoutes: Routes = [
  {
    path: "",
    component: InfopdfsPage,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild(infopdfsRoutes),
    ComponentsModule,
    TranslateModule,
    MyCommonModule,
  ],
  declarations: [InfopdfsPage],
  
})
export class InfopdfsPageModule {}
