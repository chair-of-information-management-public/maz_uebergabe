import { Component } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Observable, Subscription } from "rxjs";
import { LanguageService } from "../../language/language.service";
import {
  DocumentViewer,
  DocumentViewerOptions,
} from "@ionic-native/document-viewer/ngx";
import { switchMap } from "rxjs/operators";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { File } from "@ionic-native/File/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
@Component({
  selector: "app-infopdfs",
  templateUrl: "./infopdfs.page.html",
  providers: [InAppBrowser, FileOpener],
  styleUrls: [
    "./styles/categories.page.scss",
    "./styles/categories.shell.scss",
    "./styles/categories.responsive.scss",
  ],
})
export class InfopdfsPage {
  subscriptions: Subscription;
  pdfs: any;
  files = [];
  data: any;
  contentLoaded = false;
  constructor(
    public languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router,
    private document: DocumentViewer,
    private db: AngularFirestore,
    private inAppBrowser: InAppBrowser,
    private file: File,
    private fileOpener: FileOpener
  ) {}

  ngOnInit(): void {
    this.db
      .collection("files")
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          // doc.data() is never undefined for query doc snapshots
          let data = this.data;
          data = doc.data();
          this.files.push({
            adress: data.adress,
            filetype: data.filetype,
            publisher: data.publisher,
            title: data.title,
          });
        });
      });
  }
  /**
   * open PDF or a links
   * @param address
   */
  openPDF(address: any) {
    window.open(address);
  }
}
