import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-openvsnmodal',
  templateUrl: './openvsnmodal.page.html',
  styleUrls: ['./openvsnmodal.page.scss'],
})
export class OpenvsnmodalPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }
  dismissModal(){
    this.modalController.dismiss({
     'dismissed': true
    })
  }

}
