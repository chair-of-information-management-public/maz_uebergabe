import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpenvsnmodalPage } from './openvsnmodal.page';

const routes: Routes = [
  {
    path: '',
    component: OpenvsnmodalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpenvsnmodalPageRoutingModule {}
