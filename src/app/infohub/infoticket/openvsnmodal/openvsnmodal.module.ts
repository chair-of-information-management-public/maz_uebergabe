import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpenvsnmodalPageRoutingModule } from './openvsnmodal-routing.module';

import { OpenvsnmodalPage } from './openvsnmodal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpenvsnmodalPageRoutingModule
  ],
  declarations: [OpenvsnmodalPage]
})
export class OpenvsnmodalPageModule {}
