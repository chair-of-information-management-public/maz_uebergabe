import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OpenvsnmodalPage } from './openvsnmodal.page';

describe('OpenvsnmodalPage', () => {
  let component: OpenvsnmodalPage;
  let fixture: ComponentFixture<OpenvsnmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenvsnmodalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OpenvsnmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
