import { Component, ViewChild } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ActivatedRoute, Router } from "@angular/router";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { ModalController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import {
  VgApiService,
  VgMediaDirective,
} from "@videogular/ngx-videogular/core";
import { Subscription } from "rxjs";
import { LanguageService } from "../../language/language.service";
import { OpenvsnmodalPage } from "./openvsnmodal/openvsnmodal.page";

@Component({
  selector: "app-tickets",
  providers: [InAppBrowser],
  templateUrl: "./tickets.page.html",
  styleUrls: [
    "./styles/tickets.page.scss",
    "./styles/tickets.shell.scss",
    "./styles/tickets.responsive.scss",
  ],
})
export class TicketsPage {
  pages = [];
  data: any;
  api: VgApiService;
  urlVideo: string = "";
  isVideo = false;
  @ViewChild(VgMediaDirective, { static: true }) media: VgMediaDirective;
  items = [
    {
      title: "External file",
      url: "../../../assets/videos/Zusammenschnitt final.mp4",
      imagePreview: "assets/MAZ-Ticket.png",
    },
  ];
  constructor(
    private db: AngularFirestore,
    public languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router,
    private modalCtrl: ModalController,
    private inAppBrowser: InAppBrowser
  ) {}

  ngOnInit() {
    //move this to tickets
    this.db
      .collection("pages")
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          // doc.data() is never undefined for query doc snapshots
          let data = this.data;
          data = doc.data();
          this.pages.push({
            id: doc.id,
            name: data.name,
            icon: data.icon,
            address: data.address,
          });
        });
      });
  }
  async presentModalVSN() {
    const modal = await this.modalCtrl.create({
      component: OpenvsnmodalPage,
    });
    return await modal.present();
  }
  redirect() {
    this.inAppBrowser.create("https://vsninfo.de/de/preisauskunft");
  }
  /**
   * show playable Ticket-Video
   */
  displayVideo() {
    // show/hide video on Frontend
    this.isVideo = !this.isVideo;
  }
  onPlayerReady(api: VgApiService) {
    this.api = api;
    this.urlVideo = this.items[0].url;
  }
}
