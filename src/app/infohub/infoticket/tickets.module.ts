import { IonicModule } from "@ionic/angular";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsModule } from "../../components/components.module";

import { TicketsPage } from "./tickets.page";
import { TranslateModule } from "@ngx-translate/core";
import { MyCommonModule } from "../../my-common/my-common.module";
import { VgCoreModule } from "@videogular/ngx-videogular/core";
import { VgControlsModule } from "@videogular/ngx-videogular/controls";
import { VgOverlayPlayModule } from "@videogular/ngx-videogular/overlay-play";
import { VgBufferingModule } from "@videogular/ngx-videogular/buffering";

const ticketsRoutes: Routes = [
  {
    path: "",
    component: TicketsPage,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild(ticketsRoutes),
    ComponentsModule,
    TranslateModule,
    MyCommonModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
  ],
  declarations: [TicketsPage],
})
export class TicketsPageModule {}
