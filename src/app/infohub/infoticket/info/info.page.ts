import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireStorage } from "@angular/fire/storage";
import { Subscription } from "rxjs";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { url } from "node:inspector";

@Component({
  selector: "app-info",
  templateUrl: "./info.page.html",
  styleUrls: ["./info.page.scss"],
})
export class InfoPage implements OnInit {
  slug: string;
  page: any;
  content: string;
  imageURL: any;
  name: string;
  address: string;
  description: string;
  link: any;
  tickets: any;
  subs: Subscription;

  constructor(
    private db: AngularFirestore,
    private route: ActivatedRoute,
    private storage: AngularFireStorage
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.loadPage(params.get("slug") || "wiki");
    });
  }

  loadPage(slug) {
    if (this.subs) {
      this.subs.unsubscribe();
    }

    const doc = this.db.collection("pages").doc(slug).get();

    //read all documents from pages
    // this.db
    //   .collection("pages")
    //   .get()
    //   .subscribe((querySnapshot) => {
    //     querySnapshot.forEach((doc) => {
    //       // doc.data() is never undefined for query doc snapshots
    //     });
    //   });


    this.subs = doc.subscribe((snapshot) => {
      this.page = snapshot.data();
      if (!this.page) {
        //this.content = "### This page does not exist";
        this.slug = undefined;
      } else {
        this.slug = slug;
        //this.content = this.page.content;
        this.name = this.page.name;
        this.address = this.page.address;
        this.description = this.page.description;
        this.link = this.page.link;
        this.imageURL = this.page.imageURL;
      }
    });
  }
}
