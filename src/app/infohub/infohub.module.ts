import { IonicModule } from "@ionic/angular";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsModule } from "../components/components.module";

import { InfohubPage } from "./infohub.page";
import { TranslateModule } from "@ngx-translate/core";
import { BackButtonDirective } from "../directives/back-button.directive";

const infohubRoutes: Routes = [
  {
    path: "",
    component: InfohubPage,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild(infohubRoutes),
    ComponentsModule,
    TranslateModule,
  ],
  declarations: [InfohubPage],
})
export class InfohubPageModule {}
