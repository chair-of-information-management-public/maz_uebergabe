import { Directive, HostListener } from "@angular/core";
import { NavigationService } from "../navigation.service";

@Directive({
  selector: "[backButton]",
})
export class BackButtonDirective {
  constructor(private navigation: NavigationService) {}

  @HostListener("click") onClick() {
    this.navigation.back();
  }
}
/*
Work flow to add Back Button to a Page:
- In module.ts file: import MyCommonModule 
- Add <ion-button backButton
        ><ion-icon name="arrow-back-outline" size="large"></ion-icon
      ></ion-button> 
  to html
 */
