import { Component } from "@angular/core";
import { Plugins } from "@capacitor/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SeoService } from "./utils/seo/seo.service";
const { SplashScreen } = Plugins;
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";
import { HistoryHelperService } from "./utils/history-helper.service";
import { FirebaseAuthService } from "./firebase/auth/firebase-auth.service";
import { Storage } from "@ionic/storage";
import {
  AlertController,
  ModalController,
  Platform,
  ToastController,
} from "@ionic/angular";
import { Location } from "@angular/common";
import { LanguageService } from "./language/language.service";
import { DatenschutzPage } from "../../src/app/components/datenschutz/datenschutz.page";
import { Environment } from "./services/environment.service";
import { UserInfoService } from "./services/userInfo.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { combineLatest, Subscription } from "rxjs";
import { BulletinBoardService } from "./services/bulletin-board.service";
import { BadgeService } from "./services/badge.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: [
    "./side-menu/styles/side-menu.scss",
    "./side-menu/styles/side-menu.shell.scss",
    "./side-menu/styles/side-menu.responsive.scss",
  ],
})
export class AppComponent {
  appPages = [
    {
      title: "Categories",
      url: "/app/categories",
      ionicIcon: "list-outline",
    },
    {
      title: "Profile",
      url: "/app/user",
      ionicIcon: "person-outline",
    },
    {
      title: "Contact Card",
      url: "/contact-card",
      customIcon: "./assets/custom-icons/side-menu/contact-card.svg",
    },
    {
      title: "Notifications",
      url: "/app/notifications",
      ionicIcon: "notifications-outline",
    },
  ];
  accountPages = [
    {
      title: "Log In",
      url: "/auth/login",
      ionicIcon: "log-in-outline",
    },
    {
      title: "Sign Up",
      url: "/auth/signup",
      ionicIcon: "person-add-outline",
    },
    {
      title: "Tutorial",
      url: "/walkthrough",
      ionicIcon: "school-outline",
    },
    {
      title: "Getting Started",
      url: "/getting-started",
      ionicIcon: "rocket-outline",
    },
    {
      title: "404 page",
      url: "/page-not-found",
      ionicIcon: "alert-circle-outline",
    },
  ];

  textDir = "ltr";
  translations;
  available_languages = [];
  flag;
  name;
  obj;
  subscriptions: Subscription;

  // Inject HistoryHelperService in the app.components.ts so its available app-wide
  constructor(
    private storage: Storage,
    private router: Router,
    public translate: TranslateService,
    public historyHelper: HistoryHelperService,
    private seoService: SeoService,
    public authService: FirebaseAuthService,
    private platform: Platform,
    private location: Location,
    public languageService: LanguageService,
    private modalController: ModalController,
    private environmentService: Environment,
    private afAuth: AngularFireAuth,
    public alertController: AlertController,
    public toastController: ToastController,
    public route: ActivatedRoute,
    private bulletinBoardService: BulletinBoardService,
    private userInfoService: UserInfoService,
    private badgeService: BadgeService
  ) {
    this.initializeApp();
    this.setLanguage();
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });
    this.afAuth.onAuthStateChanged((user) => {
      let currentUser = user.uid;
      //compare total number of posts with number of posts seen by current user to determine whether there are unseen posts
      combineLatest([
        this.bulletinBoardService.getNumberOfCreatedPosts(),
        this.bulletinBoardService.getNumberOfPostsSeenByCurrUser(currentUser),
      ]).subscribe(([numberOfCreatedPosts, numOfPostsSeen]) => {
        if (numberOfCreatedPosts > numOfPostsSeen || !numOfPostsSeen) {
          this.badgeService.updateNewPostBadge(true);
        }
      });
    });
  }

  async initializeApp() {
    try {
      await SplashScreen.hide();
    } catch (err) {
      console.log("This is normal in a browser", err);
    }
  }
  async openModal() {
    const modal = await this.modalController.create({
      component: DatenschutzPage,
    });
    return await modal.present();
  }
  loadMapScript(lang) {
    const script = document.createElement("script");
    script.src =
      "https://maps.googleapis.com/maps/api/js?key=&libraries=places,directions"; //INSERT KEY HERR after "key="
    if (lang) {
      script.src += "&language=" + lang;
    }
    script.id = "google-maps-script";
    document.head.appendChild(script);
    console.log(document.getElementById("google-maps-script"));
  }
  async setLanguage() {
    // this language will be used as a fallback when a translation isn't found in the current language
    if (this.storage.get("language")) {
      const translateUse = await this.storage.get("language").then((val) => {
        if (val !== null) {
          this.translate.use(val);
          this.translate.setDefaultLang(val);
        } else this.translate.use("de");
      });
    } else {
      await this.translate.setDefaultLang("de");
      // the lang to use, if the lang isn't available, it will use the current loader to get them
      this.translate.use("de");

      // this is to determine the text direction depending on the selected language
      // for the purpose of this example we determine that only arabic and hebrew are RTL.
      // this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      //   this.textDir = (event.lang === 'ar' || event.lang === 'iw') ? 'rtl' : 'ltr';
      // });
    }
    this.loadMapScript(this.translate.defaultLang);

    this.obj = this.languageService.getFlags(this.translate.currentLang);
    this.flag = this.obj.flag;
    this.name = this.obj.name;
    this.subscriptions = this.route.data.subscribe(
      (state) => {
        this.getTranslations();
      },
      (error) => console.log(error)
    );
    this.translate.onLangChange.subscribe(() => this.getTranslations());
  }

  signOut() {
    this.authService.signOut().subscribe(
      () => {
        // Sign-out successful.
        // Replace state as we are no longer authorized to access profile page.
        this.router.navigate(["firebase/auth/sign-in"], { replaceUrl: true });
      },
      (error) => {
        console.log("signout error", error);
      }
    );
  }
  async openLanguageChooser() {
    this.available_languages = this.languageService
      .getLanguages()
      .map((item) => ({
        name: item.name,
        type: "radio",
        label: item.name,
        value: item.code,
        flag: item.flag,
        checked: item.code === this.translate.currentLang,
      }));

    const alert = await this.alertController.create({
      header: this.translations.SELECT_LANGUAGE,
      inputs: this.available_languages,
      cssClass: "language-alert",
      buttons: [
        {
          text: this.translations.CANCEL,
          role: "cancel",
          cssClass: "secondary",
          handler: () => {},
        },
        {
          text: this.translations.OK,
          handler: async (data) => {
            if (data) {
              await this.translate.use(data);
              await this.storage.set("language", data);
              this.translate.currentLang = await data;
              //this.changeGoogleMapsLanguage(data);
              this.presentToast();
            }
            this.obj = await this.languageService.getFlags(
              this.translate.currentLang
            );
            this.flag = this.obj.flag;
            this.name = this.flag.name;
            window.location.reload(); //reload app after language change
          },
        },
      ],
    });
    await alert.present();

    var radios = document.getElementsByClassName("alert-radio-label");
    for (let index = 0; index < radios.length; index++) {
      let singrad = radios[index];
      singrad.innerHTML = singrad.innerHTML.concat(
        "<img src=" +
          this.available_languages[index].flag +
          ' style="width:30px; position:absolute; right:10px;"/>'
      );
    }
  }
  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(this.translate.currentLang)
      .subscribe((translations) => (this.translations = translations));
  }
  async presentToast() {
    const Einstellungen = this.translate.instant("Einstellungen");

    const toast = await this.toastController.create({
      message: Einstellungen,
      duration: 3000,
    });
    toast.present();
  }
}
