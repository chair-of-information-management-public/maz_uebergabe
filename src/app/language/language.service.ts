import { Injectable } from "@angular/core";
import { LanguageModel } from "./language.model";

@Injectable()
export class LanguageService {
  languages: Array<LanguageModel> = new Array<LanguageModel>();

  constructor() {
    this.languages.push(
      { name: "Deutsch", code: "de", flag: "../../assets/flags/de.png" },
      { name: "український", code: "ua", flag: "../../assets/flags/ua.png" },
      { name: "Türkçe", code: "tk", flag: "../../assets/flags/tk.png" },
      { name: "Kurdî", code: "ku", flag: "../../assets/flags/ku.png" },
      { name: "فارسی", code: "fa", flag: "../../assets/flags/fa.png" },
      { name: "عربى", code: "ar", flag: "../../assets/flags/ar.png" },
      { name: "الباشتو", code: "pa", flag: "../../assets/flags/pa.png" },
      { name: "русский", code: "ru", flag: "../../assets/flags/ru.png" },
      { name: "gjuha shqipe", code: "sq", flag: "../../assets/flags/sq.png" },
      { name: "English", code: "en", flag: "../../assets/flags/gb.png" },
    );
  }

  getLanguages() {
    return this.languages;
  }
  getFlags(code: any) {
    const obj = this.languages.find((x) => x.code === code);
    return obj;
  }
}
