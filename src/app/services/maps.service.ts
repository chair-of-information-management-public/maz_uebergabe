import { Injectable } from "@angular/core";


@Injectable({
  providedIn: "root",
})
export class MapsService {
 
  /**
    * Converts the Date of the Form (String) to a Date
    * String is needed for Form Calculation, Date for API Calculation
    * @param myDate String
    * @param myMapsDate Date
    * @return [myDate: String, myMapsDate: Date]
    */
  stringToDate(myDate: any, myMapsDate: any){
    var myHour = myDate.split(":")[0];
    var myMinute = myDate.split(":")[1];
    myHour = myHour.split("T")[1];
    var myFullYear = myDate.split("T")[0];
    var myYear = this.convertStringToNumber(myFullYear.split("-")[0]);
    var myMonth = this.convertStringToNumber(myFullYear.split("-")[1]);
    var myDay = this.convertStringToNumber(myFullYear.split("-")[2]);
    var myHourN = this.convertStringToNumber(myHour);
    var myMinuteN = this.convertStringToNumber(myMinute);
    myMapsDate.setMinutes(myMinuteN);
    myMapsDate.setHours(myHourN);
    myMapsDate.setFullYear(myYear, myMonth - 1, myDay);
    return [myDate, myMapsDate]
  }

  /**
    * Converts a String to a Number
    * @param input String
    * @return Number;
    */
  convertStringToNumber(input: string) {
    var numeric = Number(input);
    return numeric;
  }

  
}
