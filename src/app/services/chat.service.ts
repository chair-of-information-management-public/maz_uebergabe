import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreCollectionGroup,
  CollectionReference,
} from "@angular/fire/firestore";
import * as firebase from "firebase/app";
import { switchMap, map } from "rxjs/operators";
import { Observable, Subscription } from "rxjs";
import { leftPad } from "angular-pipes/utils/utils";
import { BadgeService } from "./badge.service";
import { User, UserInfoService } from "./userInfo.service";
import { TranslateService } from "@ngx-translate/core";

export interface Message {
  createdAt: firebase.firestore.FieldValue;
  id: string;
  from: any;
  msg: string;
  fromName: string;
  myMsg: boolean;
  to: string;
  toName: string;
}

export interface Chat {
  messages: any;
  members: string[];
}

@Injectable({
  providedIn: "root",
})
export class ChatService {
  private currentUser;
  private chatSubscription: Subscription;
  private existingChats: Observable<any[]>;
  private newMessages: any;
  private chatid = "";
  private data: any;
  private chatsRef = this.afs.collection("chats");
  private role: any;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private badgeService: BadgeService,
    public translate: TranslateService,
    private userInfoService: UserInfoService
  ) {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user.uid;
    });
  }
  /**
   * Find a chat based on ID of the correspondent, if no chat found then create a new one
   * @param otherUserID
   * @returns Promise
   */
  setChatID(otherUserID) {
    this.chatid = "";
    return new Promise((resolve) => {
      this.chatsRef.get().subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          this.data = doc.data();
          const members = this.data.members;
          if (
            members &&
            members.includes(this.currentUser) &&
            members.includes(otherUserID)
          ) {
            this.chatid = doc.id;
            resolve(this.chatid);
          }
        });
        //if no existing chat found then create a new one
        if (this.chatid === "") {
          let currentUserID = this.currentUser;

          this.chatsRef
            .add({
              members: [currentUserID, otherUserID],
            })
            .then((docRef) => {
              this.chatid = docRef.id;
              resolve(this.chatid);
              let newMessages = this.chatsRef
                .doc(this.chatid)
                .collection("newMessages");
              newMessages.doc(currentUserID).set({
                newMessages: 0,
              });
              newMessages.doc(otherUserID).set({
                newMessages: 0,
              });
            });
        }
      });
    });
  }
  /**
   * Get the existing Chat
   * @returns Observable
   */
  getExistingChats() {
    let obj = {};
    let chatsOfCurrUserQuery = this.afs.collection("chats", (ref) =>
      ref
        .where("members", "array-contains", this.currentUser)
        // choose chat that has messages
        .where("hasMessages", "==", true)
    );
    let chatsOfCurrUser = chatsOfCurrUserQuery.valueChanges({
      idField: "id",
    }) as Observable<any[]>;

    return chatsOfCurrUser.pipe(
      switchMap((chats) => {
        chats.forEach((chat) => {
          let members = chat.members;
          let chatid = chat.id;
          members.forEach((userID) => {
            if (userID != this.currentUser) {
              obj[userID] = { chat: chat };
            }
          });
        });
        return this.userInfoService.getUsers();
      }),
      map((users) => {
        users.forEach((user) => {
          if (obj[user.uid]) obj[user.uid]["user"] = user;
        });
        return Object.values(obj);
      })
    );
  }
  /**
   * Add a message to chat
   * @param msg
   * @param otherUserID
   * @returns Promise
   */
  addChatMessage(msg, otherUserID) {
    this.chatsRef.doc(this.chatid).update({
      hasMessages: true,
    });
    this.chatsRef
      .doc(this.chatid)
      .collection("newMessages")
      .doc(otherUserID)
      .update({
        newMessages: firebase.firestore.FieldValue.increment(1),
      });
    return this.afs
      .collection("chats")
      .doc(this.chatid)
      .collection("messages")
      .add({
        msg: msg,
        from: this.currentUser,
        to: otherUserID,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      });
  }
  /**
   * Get all messages from a Chat
   * @param chatId
   * @returns Observable
   */
  getChatMessages(chatId = this.chatid) {

    let users = [];

    return this.userInfoService.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection("chats")
          .doc(chatId)
          .collection("messages", (ref) => ref.orderBy("createdAt"))
          .valueChanges({ idField: "id" }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          // m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = this.currentUser === m.from;
        }
        return messages;
      })
    );
  }

  /**
   * Mark a message as read
   * @param chatid
   */
  markAsRead(chatid = this.chatid) {
    this.afs
      .collection("chats")
      .doc(chatid)
      .collection("newMessages")
      .doc(this.currentUser)
      .update({
        newMessages: 0,
      });
  }

  getNewMessages() {
    this.existingChats = this.getExistingChats();
    this.chatSubscription = this.existingChats.subscribe((chats) => {
      chats.forEach((chatObj) => {
        let chat = chatObj.chat;
        this.afs
          .collection("chats")
          .doc(chat.id)
          .collection("newMessages")
          .doc(this.currentUser)
          .valueChanges()
          .subscribe((doc) => {
            chat.newMessages = doc.newMessages;
            this.newMessages = this.newMessages + chat.newMessages;
          });
      });
    });
    this.badgeService.updateNewIncomingMessageBadge(this.newMessages);
    return this.newMessages;
  }
}
