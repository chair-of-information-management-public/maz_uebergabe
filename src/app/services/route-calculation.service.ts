import { Injectable } from "@angular/core";

import { of, from, BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class RouteCalculationService {
  directionsService = new google.maps.DirectionsService();
  linkToAgency;
  isTransit: boolean;
  request = new BehaviorSubject<any>([]);
  response: any;
  constructor() {}
  /**
   * Set route request for Map
   * @param mode
   * @param departure
   * @param mapsDate
   * @param start
   * @param destination
   */
  setRequest(
    mode: any,
    departure: boolean,
    mapsDate: any,
    start: any,
    destination: any
  ) {
    this.request.next({
      mode: mode,
      departure: departure,
      mapsDate: mapsDate,
      start: start,
      destination: destination,
    });
  }
  /**
   * Get route request as an Observable
   * @returns Observable
   */
  getRequest() {
    return this.request.asObservable();
  }
  /**
   * Send route data to Google Direction Service for route calculation
   * @param mode
   * @param departure
   * @param mapsDate
   * @param start
   * @param destination
   * @param fromMap
   * @param fromMapDestination
   */
  async setcalculateAndDisplayRoute(
    mode: any,
    departure: boolean,
    mapsDate: any,
    start: any,
    destination: any,
    fromMap?: boolean,
    fromMapDestination?: any
  ) {

    await this.directionsService.route(
      {
        origin: start,
        destination: destination,
        travelMode: google.maps.TravelMode[mode],
        transitOptions: {
          arrivalTime: departure ? null : mapsDate,
          departureTime: departure ? mapsDate : null, 
          modes: [google.maps.TransitMode.RAIL],
        },
      },
      (response, status) => {
        if (status === "OK") {
          this.response = response;
          const steps = response.routes[0].legs[0].steps;
          let firstTransit: any, lastTransit: any;
          let first = steps.length - 1,
            last = 0;
          this.isTransit = false;
          for (var i = 0; i < steps.length; i++) {
            //send departure stop of first and arrival stop of last transits to transit agency
            let step = steps[i];
            if (step.travel_mode == "TRANSIT") {
              this.isTransit = true;
              if (i <= first) {
                firstTransit = step.transit;
                first = i;
              }
              if (i >= last) {
                lastTransit = step.transit;
                last = i;
              }
            }
          }

          //make query string
          if (this.isTransit) {
            const myUrlWithParams = new URL(
              "https://reiseauskunft.bahn.de/bin/query.exe/dn"
            );
            myUrlWithParams.searchParams.append(
              "S",
              firstTransit.departure_stop.name
            );
            myUrlWithParams.searchParams.append(
              "Z",
              lastTransit.arrival_stop.name
            );
            myUrlWithParams.searchParams.append(
              "boarding_time",
              `${firstTransit.departure_time}`
            );
            myUrlWithParams.searchParams.append(
              "arrival_time",
              `${lastTransit.arrival_time}`
            );
            myUrlWithParams.searchParams.append(
              "date", 
              `${mapsDate.getDate()}.${
                mapsDate.getMonth() + 1
              }.${mapsDate.getFullYear()}`
            );
            myUrlWithParams.searchParams.append(
              "time",
              `${firstTransit.departure_time.text}`
            );
            console.log(myUrlWithParams.href);
            this.linkToAgency = myUrlWithParams.href;
          }
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  }
  /**
   * Get map response
   * @returns any
   */
  async getResponse() {
    return this.response;
  }
  /**
   * Get travel agency URL
   * @returns string
   */
  async getlinkToAgency() {
    return this.linkToAgency;
  }
  /**
   * Check if requested travel mode is TRANSIT
   * @returns boolean
   */
  async getIsTransit() {
    return this.isTransit;
  }
}
