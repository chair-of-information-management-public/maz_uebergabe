import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase/app";
import { switchMap, map } from "rxjs/operators";
import { Observable } from "rxjs";
import { ChatService } from "./chat.service";
import { constants, UserInfo } from "os";
import { ToastController } from "@ionic/angular";
import { UserInfoService } from "./userInfo.service";
import { DatePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { LIFECYCLE_HOOKS_VALUES } from "@angular/compiler/src/lifecycle_reflector";

export interface User {
  uid: string;
  email: string;
}

export interface Route {
  start: string;
  destination: string;
  travelmode: string;
  date: Date;
  createdTime: Date;
  createdAt: firebase.firestore.FieldValue;
  mapResponse;
}

@Injectable({
  providedIn: "root",
})
export class FavoritesService {
  currentUser;
  favoriteRoutes = [];
  // otherUser: User = {
  //   uid: "xyJqQN5DU2TruC7jDM1VlgJfrRi1",
  //   email: "max.mustermann@gmail.com",
  // };
  // otherUser: User = {
  //   uid: "2shP267KdmNnbZsiN5QvEU8Gjld2",
  //   email: "behoerde@gmail.com",
  // };

  private data: any;

  private favoritesRef = this.afs.collection("favorites");
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    public userInfoService: UserInfoService,
    public datepipe: DatePipe,
    public toastController: ToastController,
    private translate: TranslateService
  ) {}
  async ngOninit() {
    await this.getCurrentUser();
  }

  /**
   * Get ID of current user
   * @returns string
   */
  async getCurrentUser() {
    await this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
    });
    return this.currentUser;
  }

  /**
   * Set a new Favorite Route
   * @param start
   * @param destination
   * @param travelmode
   * @param date
   * @param createdTime
   * @param uid
   * @param linkToAgency
   * @returns Firebase collection with favorites of the respective uid
   */
  async addFavorite(
    start,
    destination,
    travelmode,
    date,
    createdTime,
    mapsResponse,
    uid,
    linkToAgency
  ) {
    const routeAdded = this.translate.instant("RouteAdded");
    const toast = await this.toastController.create({
      message: routeAdded,
      duration: 3000,
    });
    toast.present();
    return this.afs
      .collection("favorites")
      .doc(uid)
      .collection("savedRoutes")
      .add({
        start: start,
        destination: destination,
        origin: start,
        travelmode: travelmode,
        date: date,
        createdTime: createdTime,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        mapsResponse: mapsResponse,
        linkToAgency: linkToAgency,
      });
  }
  /**
   * Delete an existing Favorite Route with a specific id
   * @param id
   * @param uid
   * @returns Firebase collection with updated Favorite Routes
   */
  async deleteFavorite(id, uid) {
    this.getCurrentUser;
    return await this.afs
      .collection("favorites")
      .doc(uid)
      .collection("savedRoutes")
      .doc(id)
      .delete();
  }

  /**
   * Initialize all Favorite Routes into local Array
   * @returns void
   */
  async initializeItems() {
    this.currentUser = await this.userInfoService.getCurrentUser(); // get current user ID
    this.favoriteRoutes = [];
    this.afs
      .collection("favorites")
      .doc(this.currentUser)
      .collection("savedRoutes", (ref) => ref.orderBy("date"))
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          doc.data();
          let data = this.data;
          data = doc.data();
          this.favoriteRoutes.push({
            id: doc.id,
            datePiped: this.datepipe.transform(
              data.date.toDate(),
              "dd.MM.yyyy - HH:mm"
            ),
            dateOrig: data.date.toDate(),
            from: data.origin,
            createdTime: this.datepipe.transform(
              data.createdTime.toDate(),
              "dd.MM.yyyy - HH:mm"
            ),
            to: data.destination,
            imageSRC: data.mapsResponse,
            staticMapImage: data.staticMapImage,
            linkToAgency: data.linkToAgency,
          });
        });
        for (let route of this.favoriteRoutes) {
          route.staticMapImage =
            "https://maps.googleapis.com/maps/api/staticmap?&size=600x400&path=enc:" +
            route.imageSRC +
            "&key="; //INSERT KEY HERE
        }
      });

    return this.favoriteRoutes;
  }
  /**
   * Get all Favorite Routes of a specific User
   * @param uid
   * @returns Firebase collection with favorites of the respective uid
   */
  async getFavorites(uid) {
    return this.afs
      .collection("favorites")
      .doc(uid)
      .collection("savedRoutes")
      .valueChanges({ idField: "route" });
  }

  /**
   * Set a post as a favaorite
   * @param uid
   * @param postId
   * @returns Firebase collection with liked posts of the respective uid, with updated likes
   */

  addLike(uid, postId) {
    return this.afs
      .collection("favorites")
      .doc(uid)
      .collection("likedPosts")
      .doc(postId)
      .set({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      });
  }

  /**
   * Get liked posts of a specific user
   * @param uid
   * @returns Firebase collection with liked posts of the respective ui
   */

  getLikes(uid) {
    return this.afs
      .collection("favorites")
      .doc(uid)
      .collection("likedPosts")
      .ref.get();
    // .valueChanges({ idField: "postId" });
  }

  /**
   * Unlike a post of a specific user
   * @param uid
   * @param postId
   * @returns Firebase collection with liked posts of the respective ui, with updated likes
   */
  unLike(uid, postId) {
    return this.afs
      .collection("favorites")
      .doc(uid)
      .collection("likedPosts")
      .doc(postId)
      .delete();
  }
  /**
   * Examine if a specific post is liked by a specific user
   * @param uid
   * @param postId
   * @returns Promise
   */

  async isLiked(postId, uid): Promise<boolean> {
    const likesRef = this.afs
      .collection("favorites")
      .doc(uid)
      .collection("likedPosts")
      .doc(postId).ref;

    return await likesRef.get().then((docSnapshot) => {
      if (docSnapshot.exists) {
        return true;
      }
      return false;
    });
  }
}
