import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ChatService } from "./chat.service";
import { User, UserInfoService } from "./userInfo.service";
import * as firebase from "firebase/app";
import { BehaviorSubject, combineLatest, Observable } from "rxjs";
import { FavoritesService } from "./favorites.service";
import { resolve } from "core-js/fn/promise";
import {
  distinctUntilChanged,
  filter,
  last,
  map,
  pluck,
  takeLast,
} from "rxjs/operators";
export interface Post {
  postId: string;
  content: string;
  departureTime: any; //1
  start: any; //2
  destination: any; //2
  language: string; //3
  numberOfSeats: number; //3
  numberOfComments: number;
  author: string;
  createdAt: firebase.firestore.FieldValue;
  authorId?: string;
  authorAvatar?: any;
  seatsTaken: number;
  userOfSeatsTaken: any[];
}
export interface Comment {
  commentId: string;
  author: string;
  content: string;
  createdAt: firebase.firestore.FieldValue;
}
@Injectable({
  providedIn: "root",
})
export class BulletinBoardService {
  currentUser;
  postRef = this.afs.collection("posts", (ref) =>
    ref.orderBy("createdAt", "desc")
  );
  postSubscription: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  constructor(
    private chatService: ChatService,
    private afs: AngularFirestore,
    private userInfo: UserInfoService,
    private favoriteService: FavoritesService
  ) {
    // this.userInfo.getCurrentUser().then((user) => {
    //   this.currentUser = user;
    // });
  }
  /**
   * Create a new Post for Bulletin Board Page
   * @param content
   * @param departureTime
   * @param start
   * @param destination
   * @param language
   * @param numberOfSeats
   * @param numberOfComments
   * @param userOfSeatsTaken
   * @returns Promise
   */
  async createPost(
    content,
    departureTime,
    start,
    destination,
    language,
    numberOfSeats,
    numberOfComments,
    userOfSeatsTaken
  ) {
    this.currentUser = await this.userInfo.getCurrentUser();
    return this.postRef.add({
      content: content,
      departureTime: departureTime,
      start: start,
      destination: destination,
      language: language,
      numberOfSeats: numberOfSeats,
      numberOfComments: numberOfComments,
      authorId: this.currentUser,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      seatsTaken: 0,
      userOfSeatsTaken: [],
    });
  }
  /**
   * Fetch all posts
   * @returns Observable
   */
  getPosts(lastDoc) {
    if (lastDoc) {
      return this.postRef.ref
        .orderBy("createdAt", "desc")
        .startAfter(lastDoc)
        .limit(10)
        .get();
    }
    return this.postRef.ref.orderBy("createdAt", "desc").limit(10).get();
  }
  getPostsRealTime() {
    return this.postRef.valueChanges() as Observable<Post[]>;
  }
  /**
   * Fetch all liked posts
   * @param post_ids
   * @returns Observable
   */
  getLikedPosts(post_ids) {
    return this.afs
      .collection("posts", (ref) => ref.where("postId", "in", post_ids))
      .valueChanges() as Observable<Post[]>;
  }
  /**
   * Fetch a post from its ID
   * @param postId
   * @returns Promise
   */
  async getPostFromId(postId) {
    return await (await this.postRef.doc(postId).ref.get()).data();
  }
  /**
   * Update a Post
   * @param authorId
   * @param postId
   * @param content
   * @param departureTime
   * @param start
   * @param destination
   * @param language
   * @param numberOfSeats
   * @param numberOfComments
   * @returns Promise
   */
  async updatePost(
    authorId,
    postId,
    content,
    departureTime,
    start,
    destination,
    language,
    numberOfSeats,
    numberOfComments
  ) {
    this.currentUser = await this.userInfo.getCurrentUser();
    if (this.currentUser !== authorId) return;

    await this.postRef.doc(postId).update({
      content: content,
      departureTime: departureTime,
      start: start,
      destination: destination,
      language: language,
      numberOfSeats: numberOfSeats,
      numberOfComments: numberOfComments++,
    });
    this.postSubscription.next({ postId: postId, method: "update" });
  }
  /**
   * Delete a Post
   * @param authorId
   * @param postId
   * @returns Promise
   */
  async deletePost(authorId, postId) {
    this.currentUser = await this.userInfo.getCurrentUser();
    if (this.currentUser !== authorId) return;
    // this.postSubscription.next({ postId: postId, method: "delete" });
    await this.postRef.doc(postId).delete();
    this.postRef
      .doc("numberOfPosts")
      .update({ numberOfPosts: firebase.firestore.FieldValue.increment(-1) });
    this.postSubscription.next({ postId: postId, method: "delete" });
  }

  /**
   * Create a Comment
   * @param postId
   * @param content
   * @returns Promise
   */
  async createComment(postId, content) {
    this.currentUser = await this.userInfo.getCurrentUser();
    return this.postRef.doc(postId).collection("comments").add({
      authorId: this.currentUser,
      content: content,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    });
  }
  /**
   * Fetch all Comments
   * @param postId
   * @returns Observable
   */
  getComments(postId) {
    return this.postRef
      .doc(postId)
      .collection("comments", (ref) => ref.orderBy("createdAt"))
      .valueChanges({ idField: "commentId" }) as Observable<Comment[]>;
  }
  /**
   * Update a Comment
   * @param authorId
   * @param postId
   * @param commentId
   * @param content
   * @returns Promise
   */
  async updateComment(authorId, postId, commentId, content) {
    this.currentUser = await this.userInfo.getCurrentUser();
    if (this.currentUser !== authorId) return;
    return this.postRef
      .doc(postId)
      .collection("comments")
      .doc(commentId)
      .update({
        content: content,
      });
  }
  /**
   * Update number of Comments
   * @param postId
   * @returns Promise
   */
  updateNumberOfComment(postId) {
    const increment = firebase.firestore.FieldValue.increment(1);
    return this.postRef.doc(postId).update({ numberOfComments: increment });
  }
  /**
   * Update number of seats already be taken in a ride
   * @param postId
   * @param uid
   * @param incrementAmount
   * @returns Promise
   */
  async updateNumberOfSeatsTaken(postId, uid, incrementAmount) {
    await this.updateUserOfSeatsTaken(postId, uid, incrementAmount);
    const increment = firebase.firestore.FieldValue.increment(incrementAmount);
    await this.postRef.doc(postId).update({ seatsTaken: increment });
    this.postSubscription.next({ postId: postId, method: "update" });
  }
  /**
   * Add or remove user from a ride
   * @param postId
   * @param uid
   * @param incrementAmount
   * @returns Promise
   */
  updateUserOfSeatsTaken(postId, uid, incrementAmount) {
    if (incrementAmount > 0) {
      return this.postRef.doc(postId).update({
        userOfSeatsTaken: firebase.firestore.FieldValue.arrayUnion(uid),
      });
    }
    if (incrementAmount < 0) {
      return this.postRef.doc(postId).update({
        userOfSeatsTaken: firebase.firestore.FieldValue.arrayRemove(uid),
      });
    }
  }
  /**
   * Delete a Comment
   * @param authorId
   * @param postId
   * @param commentId
   * @returns Promise
   */
  async deleteComment(authorId, postId, commentId) {
    this.currentUser = await this.userInfo.getCurrentUser();
    if (this.currentUser !== authorId) return;
    const decrement = firebase.firestore.FieldValue.increment(-1);
    this.postRef.doc(postId).update({ numberOfComments: decrement });
    return this.postRef
      .doc(postId)
      .collection("comments")
      .doc(commentId)
      .delete();
  }

  /**
   * subscribe to changes of a single post
   * @returns Observable
   */
  getPostSubscription() {
    return this.postSubscription.asObservable();
  }

  /**
   * listen to total number of posts
   * @returns Observable
   */
  getNumberOfPosts() {
    return this.postRef
      .doc("numberOfPosts")
      .valueChanges()
      .pipe(pluck("numberOfPosts"));
  }

  /**
   * listen to total number of created posts (number of created posts - number of deleted posts = number of posts)
   * @returns Observable
   */
  getNumberOfCreatedPosts() {
    return this.postRef
      .doc("numberOfCreatedPosts")
      .valueChanges()
      .pipe(pluck("numberOfCreatedPosts"));
  }

  /**
   * Returns the number posts seen by a certain user
   * @param currentUser
   * @returns Observable
   */
  getNumberOfPostsSeenByCurrUser(currentUser) {
    return this.afs
      .collection("normalUser")
      .doc(currentUser)
      .valueChanges()
      .pipe(pluck("numberOfPostsSeen"));
  }

  /**
   * Update the number of post seen by the current user
   * @param numOfPosts
   * @returns Promise
   */
  async setNumberOfPostsSeenByCurrUser(numOfPosts) {
    this.currentUser = await this.userInfo.getCurrentUser();

    return this.afs
      .collection("normalUser")
      .doc(this.currentUser)
      .update({ numberOfPostsSeen: numOfPosts });
  }
}
