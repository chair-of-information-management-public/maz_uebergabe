import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase/app";
import { switchMap, map } from "rxjs/operators";
import { Observable } from "rxjs";

export interface Logo {
  uid: string;
  email: string;
}


@Injectable({
  providedIn: "root",
})
export class LogoService {
/**
   * Gets universal logo for "Integration"
   * @returns Name for the logo of "Integration"
   */
  getIntegrationLogo() {
  return "chatbox-outline";
  }
/**
   * Gets universal color for "Integration"
   * @returns Name for the color of "Integration"
   */
  getIntegrationColor(){
    return "danger";
  }
/**
   * Gets universal logo for "Behoerde"
   * @returns Name for the logo of "Behoerde"
   */
  getBehoerdeLogo() {
  return "business-outline";
  }
/**
   * Gets universal color for "Behoerde"
   * @returns Name for the color of "Behoerde"
   */
  getBehoerdeColor(){
  return "warning";
  }
/**
   * Gets universal logo for "Mobility"
   * @returns Name for the logo of "Mobility"
   */
  getMobilityLogo() {
  return "car-outline";
  }
/**
   * Gets universal color for "Mobility"
   * @returns Name for the color of "Mobility"
   */
  getMobilityColor() {
  return "success";
    }
/**
   * Gets universal logo for "Freetime"
   * @returns Name for the logo of "Freetime"
   */
  getFreetimeLogo() {
  return "happy-outline";
  }
/**
   * Gets universal color for "Freetime"
   * @returns Name for the color of "Freetime"
   */
  getFreetimeColor() {
    return "tertiary";
    }

}
