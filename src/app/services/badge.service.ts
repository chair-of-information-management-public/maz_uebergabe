import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Storage } from "@ionic/storage";
@Injectable({
  providedIn: "root",
})
export class BadgeService {
  private favoriteAdded: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(null);
  private newMessage: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    null
  );
  private newIncomingMessage: BehaviorSubject<number> =
    new BehaviorSubject<number>(null);
  private newPostAdded: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    null
  );
  constructor(private storage: Storage) {}
  ngOnInit() {
    /**
     * Gets value for new added favorites
     * Adds value to the respective BehaviorSubject
     */
    this.storage.get("favoriteAdded").then((res) => {
      this.favoriteAdded.next(res);
    });

    /**
     * Gets value for new added messages
     * Adds value to the respective BehaviorSubject
     */
    this.storage.get("newMessage").then((res) => {
      this.newMessage.next(res);
    });

    /**
     * Gets value for incoming unread messages
     * Adds value to the respective BehaviorSubject
     */
    this.storage.get("newIncomingMessage").then((res) => {
      this.newIncomingMessage.next(res);
    });
  }
  /**
   * Updates the value for new incoming messages
   * Adds value to the respective BehaviorSubject
   * @param messages
   * @return void
   */
  updateNewIncomingMessageBadge(messages: number): void {
    this.storage.set("favoriteAdded", messages);
    this.newIncomingMessage.next(messages);
  }

  /**
   * Gets current value for added favorites
   * @return Observable
   */
  getNewIncomingMessageBadge(): Observable<number> {
    return this.newIncomingMessage;
  }

  /**
   * Updates the value for favorite added badges
   * Adds value to the respective BehaviorSubject
   * @param badge
   * @return void
   */
  updateFavoriteBadge(badge: boolean): void {
    this.storage.set("favoriteAdded", badge);
    this.favoriteAdded.next(badge);
  }

  /**
   * Gets current value for added favorites
   * @return Observable
   */
  getFavoriteBadge(): Observable<boolean> {
    return this.favoriteAdded;
  }

  /**
   * Updates the value for message badges
   * Adds value to the respective BehaviorSubject
   * @param newMessage
   * @return void
   */
  updateMessageBadge(newMessage: boolean): void {
    this.storage.set("newMessage", newMessage);
    this.newMessage.next(newMessage);
  }

  /**
   * Gets current value for new message
   * @return Observable
   */
  getMessageBadge(): Observable<boolean> {
    return this.newMessage;
  }

  /**
   * Updates the value for favorite added badges
   * Adds value to the respective BehaviorSubject
   * @param badge
   * @return void
   */
  updateNewPostBadge(badge: boolean): void {
    this.newPostAdded.next(badge);
  }

  /**
   * Gets current value for added favorites
   * @return Observable
   */
  getNewPostBadge(): Observable<boolean> {
    return this.newPostAdded;
  }
}
