import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase/app";
import { switchMap, map } from "rxjs/operators";
import { from, Observable, of } from "rxjs";
import { FirebaseApp } from "@angular/fire";
import { FirebaseAuthService } from "../firebase/auth/firebase-auth.service";
import { TranslateService } from "@ngx-translate/core";
import { waitUntil } from "async-wait-until";
export interface User {
  uid: string;
  email: string;
  displayName?: string;
  photoURL?: any;
}

@Injectable({
  providedIn: "root",
})
export class UserInfoService {
  public currentUser: string;
  private role: any;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    public authService: FirebaseAuthService,
    public translate: TranslateService
  ) {}

  /**
   * Get ID of current user
   * @returns string
   */
  async getCurrentUser() {
    await this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user.uid;
    });
    return this.currentUser;
  }

  /**
   * Get all users
   * @returns Observable
   */
  getUsers() {
    //if current user is normalUser => get users from Initiativ
    if (this.role == "NORMALUSER") {
      return this.afs
        .collection("pages", (ref) => ref.where("uid", "!=", ""))
        .valueChanges() as Observable<any[]>;
    }
    //if current user is Initiativ => get users from normalUser
    else if (this.role == "INITIATIVE")
      return this.afs.collection("normalUser").valueChanges() as Observable<
        any[]
      >;
  }
  /**
   * Get role of current user
   * @returns Promise
   */
  async getUserRole() {
    return new Promise(async (resolve) => {
      var id = await this.getCurrentUser();
      this.afs
        .collection("pages")
        .get()
        .subscribe((alldocs) => {
          let data: any;
          alldocs.forEach(function (doc) {
            data = doc.data();
            //data has uid field!
            if (data.uid == id) {
              // document exists
              resolve("INITIATIVE");
            }
          });
        });
      this.afs
        .collection("normalUser")
        .get()
        .subscribe((alldocs) => {
          alldocs.forEach(function (doc) {
            var uid = doc.id;
            if (uid == id) {
              // document exists
              resolve("NORMALUSER");
              // isNormalUser = true;
            }
          });
        });
    });
  }
  /**
   * Set current user role
   * @param role
   */
  setUserRole(role) {
    this.role = role;
  }
  /**
   * Get username and profile photo URL from uid
   * @param uid
   * @returns Promise
   */
  async getUserInfoFromId(uid) {
    let username;
    let photoURL;
    // await this.getUserRole().then((role) => {
    //   if (role == "NORMALUSER") {
    this.afs
      .collection("normalUser")
      .doc(uid)
      .ref.get()
      .then((doc) => {
        if (doc.exists) {
          let data: any = doc.data();
          username = data.username;
          photoURL = data.avatar;
        } else {
          this.afs
            .collection("pages")
            .ref.get()
            .then((snapShot) => {
              snapShot.forEach((doc) => {
                let data: any = doc.data();
                if (data.uid == uid) {
                  username = data["name_" + this.translate.currentLang];
                  photoURL = data.icon;
                }
              });
            });
        }
      });
    // } else {

    // }
    // });
    await waitUntil(() => username != null && photoURL != null);

    return { username, photoURL };
  }
  /**
   * Set username
   * @param uid
   * @param newUserName
   */
  setUsername(uid, newUserName) {
    this.afs
      .collection("normalUser")
      .doc(uid)
      .ref.get()
      .then((doc) => {
        if (doc.exists) {
          this.afs
            .collection("normalUser")
            .doc(uid)
            .update({ username: newUserName });
        }
      });
    this.afs
      .collection("pages")
      .doc(uid)
      .ref.get()
      .then((doc) => {
        if (doc.exists) {
          let name = "name_" + this.translate.currentLang;
          this.afs
            .collection("pages")
            .doc(uid)
            .update({ [name]: newUserName });
        }
      });
  }
}
