import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase/app";
import { switchMap, map } from "rxjs/operators";
import { Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: "root",
})
export class Environment {
  private selectedPage;
  private arrivalTime;
  private selected_start;
  private selected_dest;
  private loading;

  constructor(
    private router: Router,
    private loadingController: LoadingController,
    public translate: TranslateService
  ) {}
  /**
   * Set selected page from Map
   * @param page
   */
  setSelectedPage(page: any) {
    this.selectedPage = page;
  }
  /**
   * Get selected page from Map
   * @returns any
   */
  getSelectedPage() {
    return this.selectedPage;
  }
  /**
   * Set selected start from Map
   * @param page
   */
  setSelectedStart(page: any) {
    this.selected_start = page;
  }
  /**
   * Get selected start from Map
   * @returns any
   */
  getSelectedStart() {
    return this.selected_start;
  }
  /**
   * Set selected destination from Map
   * @param page
   */
  setSelectedDest(page: any) {
    this.selected_dest = page;
  }
  /**
   * Get selected destination from Map
   * @returns any
   */
  getSelectedDest() {
    return this.selected_dest;
  }
  /**
   * Set arrival time from Map
   * @param arrivalTime
   */
  setArrivalTime(arrivalTime) {
    this.arrivalTime = arrivalTime;
  }
  /**
   * Get arrival time from Map
   * @returns any
   */
  getArrivalTime() {
    return this.arrivalTime;
  }
  /**
   * Send request to map to calculate route
   * @param from
   * @param to
   * @param departureTime
   * @param mode
   * @param isDeparture
   */
  sendRouteDataToMap(
    from,
    to,
    departureTime?: any,
    mode?: any,
    isDeparture?: any
  ) {
    console.log(departureTime);
    this.router.navigate(["app/maps/maps"], {
      state: {
        route: {
          from: from,
          to: to,
          departureTime: departureTime,
          mode: mode,
          isDeparture: isDeparture,
        },
      },
    });
  }

  async createLoader(callback: () => Promise<void>, LoaderName?) {
    if (!LoaderName) LoaderName = "Loading";
    this.loading = await this.loadingController.create({
      cssClass: "my-loading-class",
      translucent: true,
      backdropDismiss: true,
      message: this.translate.instant(LoaderName),
      // duration: 0,
    });
    await this.loading.present();
    await callback();
    this.dismissLoader();
  }

  async dismissLoader() {
    if (this.loading) await this.loading.dismiss();
  }
}
