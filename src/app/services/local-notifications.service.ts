import { trigger } from '@angular/animations';
import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { TranslateService } from "@ngx-translate/core";
@Injectable({
  providedIn: 'root'
})
export class LocalNotificationsService {

  constructor(
    private localNotification: LocalNotifications,
    public translate: TranslateService,) { }

/**
   * Set a notification for a user at a specific date and with a specific text
   * @param triggerDate
   * @param notificationAdress
   * @returns voide
   */
  scheduleNotification(triggerDate:Date, notificationAdress){
    const notificationTextBlocks = this.setNotificationText(notificationAdress, triggerDate)
    console.log("Scheduled Notification for " + triggerDate.toString());
    this.localNotification.schedule(
      {
      title: notificationTextBlocks[0],
      icon: 'https://icon-library.com/images/route-icon/route-icon-14.jpg',
      text: notificationTextBlocks[1],
      trigger: {at: triggerDate},
      vibrate: true,
      id: Number(triggerDate),
      smallIcon: 'https://github.com/TillDies/kmldata/blob/main/favicon.png?raw=true'
    });
  }

  /**
   * Rewrite the notification Address to a readable notification text
   * @param notificationAdress
   * @param notificationDate
   * @returns Rewritten notification Title and notification Text
   */
  setNotificationText(notificationAdress, notificationDate){
    var notificationTitle = this.translate.instant("NotificationTitle")
    var notificationText = this.translate.instant("NotificationText")
    var notificationText2 = this.translate.instant("NotificationText2")
    notificationText = notificationText + notificationAdress + notificationText2 + notificationDate.getHours() + ":" + notificationDate.getMinutes() +"."
    return [notificationTitle, notificationText]
  }
 /**
   * Cancels an existing notification
   * @param id
   * @returns void
   */
  cancelNotification(id: Number){
    this.localNotification.cancel(id);
  }
}
