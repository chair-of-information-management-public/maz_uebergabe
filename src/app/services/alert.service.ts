import { Injectable } from "@angular/core";
import { AlertController } from "@ionic/angular";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class AlertService {
 
    constructor (public alertController: AlertController, public router: Router){}

    /**
    * presents Alert with Cancel and GoTo-Button
    * @param string header
    * @param string message
    * @param string textButton1
    * @param string textButton2
    * @param string LinkToSite
    * @return void
    */
    async presentAlertGoTo(header: any, message: any, textButton1: any, textButton2: any, link: any) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: header,
          message: message,
          buttons: [
            {
              text: textButton1,
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                console.log('Confirm Cancel');
              }
            }, {
              text: textButton2,
              handler: () => {
               this.router.navigate([link])
              }
            }
          ]
        });
    
        await alert.present();
      }
   /**
    * presents Alert with Cancel and GoTo-Button
    * @param string textHeader
    * @param string textMessage
    * @param string textButton
    * @return void
    */

      async presentAlertFavorite(textHeader: any, textMessage: any, textButton: any) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: textHeader,
          message: textMessage,
          buttons: [textButton]
        });
    
        await alert.present();
    
        const { role } = await alert.onDidDismiss();
        console.log('onDidDismiss resolved with role', role);
      }

}
