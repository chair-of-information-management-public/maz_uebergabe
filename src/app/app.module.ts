import {
  APP_INITIALIZER,
  NgModule,
  Optional,
  PLATFORM_ID,
} from "@angular/core";
import {
  BrowserModule,
  BrowserTransferStateModule,
} from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { ServiceWorkerModule } from "@angular/service-worker";
import { RESPONSE } from "@nguniversal/express-engine/tokens";
import { DatePipe, isPlatformServer } from "@angular/common";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ComponentsModule } from "./components/components.module";
import { environment } from "../environments/environment";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { VgCoreModule } from "@videogular/ngx-videogular/core";
import { VgControlsModule } from "@videogular/ngx-videogular/controls";
import { VgOverlayPlayModule } from "@videogular/ngx-videogular/overlay-play";
import { VgStreamingModule } from "@videogular/ngx-videogular/streaming";
import { VgBufferingModule } from "@videogular/ngx-videogular/buffering";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import {
  TranslateModule, 
  TranslateLoader,
  TranslatePipe,
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
  AngularFireAuthGuard,
  AngularFireAuthGuardModule,
} from "@angular/fire/auth-guard";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuth, AngularFireAuthModule } from "@angular/fire/auth";
import { FirebaseAuthService } from "./firebase/auth/firebase-auth.service";
import { FirebaseAuthModule } from "./firebase/auth/firebase-auth.module";
import { ImagePicker } from "@ionic-native/image-picker/ngx";
import { File } from "@ionic-native/File/ngx";
import { MediaCapture } from "@ionic-native/media-capture/ngx";
import { Media } from "@ionic-native/media/ngx";
import { StreamingMedia } from "@ionic-native/streaming-media/ngx";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";
import { LanguageService } from "./language/language.service";
import { IonicStorageModule } from "@ionic/storage";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { DatenschutzPageModule } from "./components/datenschutz/datenschutz.module";
import { MarkdownModule } from "ngx-markdown";
import { NgxAutocomPlaceModule } from "ngx-autocom-place";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx/index";
import { NavigationService } from "./navigation.service";
import { DocumentViewer } from "@ionic-native/document-viewer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IonicSelectableModule } from "ionic-selectable";
import { LocalNotifications } from "@ionic-native/local-notifications/ngx";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    BrowserTransferStateModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    MarkdownModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicSelectableModule,
    FormsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    VgStreamingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    ComponentsModule,
    FirebaseAuthModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    NgxAutocomPlaceModule,
    AngularFireStorageModule,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AngularFireAuthGuard,
    {
      provide: APP_INITIALIZER,
      useFactory: (platformId: object, response: any) => {
        return () => {
          // In the server.ts we added a custom response header with information about the device requesting the app
          if (isPlatformServer(platformId)) {
            if (response && response !== null) {
              // Get custom header from the response sent from the server.ts
              const mobileDeviceHeader = response.get("mobile-device");

              // Set Ionic config mode?
            }
          }
        };
      },
      deps: [PLATFORM_ID, [new Optional(), RESPONSE]],
      multi: true,
    },
    AngularFireAuth,
    ImagePicker,
    LocalNotifications,
    MediaCapture,
    DatePipe,
    File,
    InAppBrowser,
    Media,
    StreamingMedia,
    PhotoViewer,
    LanguageService,
    FileOpener,
    NavigationService,
    DocumentViewer,
    Diagnostic
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
