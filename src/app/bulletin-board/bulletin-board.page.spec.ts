import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BulletinBoardPage } from './bulletin-board.page';

describe('BulletinBoardPage', () => {
  let component: BulletinBoardPage;
  let fixture: ComponentFixture<BulletinBoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulletinBoardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BulletinBoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
