import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { BulletinBoardPageRoutingModule } from "./bulletin-board-routing.module";

import { BulletinBoardPage } from "./bulletin-board.page";
import { TranslateModule } from "@ngx-translate/core";
import { ComponentsModule } from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    BulletinBoardPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [BulletinBoardPage],
})
export class BulletinBoardPageModule {}
