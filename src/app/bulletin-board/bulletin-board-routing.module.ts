import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BulletinBoardPage } from "./bulletin-board.page";

const routes: Routes = [
  {
    path: "",
    component: BulletinBoardPage,
  },
  {
    path: "post-edit",
    loadChildren: () =>
      import("./post-edit/post-edit.module").then((m) => m.PostEditPageModule),
  },
  {
    path: 'comments',
    loadChildren: () => import('./comments/comments.module').then( m => m.CommentsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BulletinBoardPageRoutingModule {}
