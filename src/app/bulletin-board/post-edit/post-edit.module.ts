import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PostEditPageRoutingModule } from "./post-edit-routing.module";

import { PostEditPage } from "./post-edit.page";
import { NgxAutocomPlaceModule } from "ngx-autocom-place";
import { TranslateModule } from "@ngx-translate/core";
import { IonicSelectableModule } from "ionic-selectable";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostEditPageRoutingModule,
    ReactiveFormsModule,
    NgxAutocomPlaceModule,
    TranslateModule,
    IonicSelectableModule
  ],
  declarations: [PostEditPage],
})
export class PostEditPageModule {}
