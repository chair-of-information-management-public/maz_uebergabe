import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { FormBuilder } from "@angular/forms";
import { LanguageService } from "../../language/language.service";
import { Validators } from "@angular/forms";
import { BulletinBoardService } from "../../services/bulletin-board.service";
import { TranslateService } from "@ngx-translate/core";
import { IonicSelectableComponent } from "ionic-selectable";
import { UserInfoService } from "../../services/userInfo.service";
import { Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import firebase from "firebase";

@Component({
  selector: "app-post-edit",
  templateUrl: "./post-edit.page.html",
  styleUrls: ["./post-edit.page.scss"],
})
export class PostEditPage implements OnInit {
  private currentUser;
  departureTime = new Date().toISOString().substring(0, 16);
  available_languages = [];
  chosenLang = undefined;
  flag;
  numberOfComments = 0;
  userOfSeatsTaken = [];
  name;
  choosenLang;
  obj;
  uid;
  post;
  postForm = this.fb.group({
    content: [""],
    start: ["", Validators.required],
    destination: ["", Validators.required],
    departureTime: [this.departureTime, Validators.required],
    language: [""],
    numberOfSeats: ["", Validators.required],
  });
  postRef = this.afs.collection("posts", (ref) =>
    ref.orderBy("createdAt", "desc")
  );
  @ViewChild("langChoser") contactList: IonicSelectableComponent;
  @Input() isEdit: boolean; //true: edit posr, false: create post
  @Input() postId: string;

  constructor(
    private modalController: ModalController,
    private fb: FormBuilder,
    public translate: TranslateService,
    public alertController: AlertController,
    public languageService: LanguageService,
    private bulletinBoardService: BulletinBoardService,
    private userInfoService: UserInfoService,
    private router: Router,
    private afs: AngularFirestore
  ) {}

  async ngOnInit() {
    this.currentUser = await this.userInfoService.getCurrentUser();
    this.available_languages = this.languageService.getLanguages();
    if (this.isEdit) {
      this.post = await this.bulletinBoardService.getPostFromId(this.postId);
      this.postForm.patchValue({
        content: this.post.content,
        start: this.post.start,
        destination: this.post.destination,
        departureTime: this.post.departureTime,
        language: this.post.language,
        numberOfSeats: this.post.numberOfSeats,
      });
    }
    this.uid = await this.userInfoService.getCurrentUser();
  }
  dismissModal(newPostId?) {
    this.modalController.dismiss({
      dismissed: true,
      newPostId: newPostId,
    });
  }
  async createPost() {
    let docRef = await this.bulletinBoardService.createPost(
      this.postForm.value.content,
      this.postForm.value.departureTime,
      this.postForm.value.start,
      this.postForm.value.destination,
      this.postForm.value.language,
      this.postForm.value.numberOfSeats,
      this.numberOfComments,
      this.userOfSeatsTaken
    );
    this.postRef.doc(docRef.id).update({ postId: docRef.id });
    this.postRef
      .doc("numberOfPosts")
      .update({ numberOfPosts: firebase.firestore.FieldValue.increment(1) });
    this.postRef
      .doc("numberOfCreatedPosts")
      .update({
        numberOfCreatedPosts: firebase.firestore.FieldValue.increment(1),
      });
    this.afs
      .collection("normalUser")
      .doc(this.currentUser)
      .update({
        numberOfPostsSeen: firebase.firestore.FieldValue.increment(1),
      });
    this.dismissModal(docRef.id);
  }
  async updatePost() {
    await this.bulletinBoardService.updatePost(
      this.uid,
      this.postId,
      this.postForm.value.content,
      this.postForm.value.departureTime,
      this.postForm.value.start,
      this.postForm.value.destination,
      this.postForm.value.language,
      this.postForm.value.numberOfSeats,
      this.post.numberOfComments
    );
    this.dismissModal();
  }
  savePost() {
    if (this.isEdit) {
      this.updatePost();
    } else this.createPost();
  }

  placeChangedStart($event) {
    this.postForm.patchValue({ start: $event.formatted_address });
  }
  placeChangedDestination($event) {
    this.postForm.patchValue({ destination: $event.formatted_address });
  }
}
