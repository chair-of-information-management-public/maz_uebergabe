import { templateSourceUrl } from "@angular/compiler";
import { Component, OnInit, ViewChild } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { IonContent, ModalController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import waitUntil from "async-wait-until";
import { timeStamp } from "console";
import { _ } from "core-js";
import { analytics } from "firebase";
import { combineLatest, Observable, Subscription } from "rxjs";
import { map, take } from "rxjs/operators";
import { BadgeService } from "../services/badge.service";
import { BulletinBoardService } from "../services/bulletin-board.service";
import { Environment } from "../services/environment.service";
import { FavoritesService } from "../services/favorites.service";
import { UserInfoService } from "../services/userInfo.service";
import { PostEditPage } from "./post-edit/post-edit.page";

@Component({
  selector: "app-bulletin-board",
  templateUrl: "./bulletin-board.page.html",
  styleUrls: ["./bulletin-board.page.scss"],
})
export class BulletinBoardPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  posts = [];
  currentUser: string;
  opts = {
    slidesPerView: 4.5,
    spaceBetween: 10,
    slidesOffsetBefore: 0,
  };
  isLikedPostEmpty = false;
  viewMode = "all-post";
  fav_posts = [];
  numberOfPosts: number;
  lastDoc: any;
  loading: HTMLIonLoadingElement;
  numberOfCreatedPosts: number;
  refreshing: boolean = false;

  // temp;
  constructor(
    private afAuth: AngularFireAuth,
    private bulletinBoardService: BulletinBoardService,
    public modalController: ModalController,
    private userInfo: UserInfoService,
    private favoriteService: FavoritesService,
    private afs: AngularFirestore,
    private environmentService: Environment,
    private badgeService: BadgeService
  ) {}

  async ngOnInit() {
    this.currentUser = await this.userInfo.getCurrentUser();
    this.bulletinBoardService.getNumberOfPosts().subscribe((res: any) => {
      this.numberOfPosts = res;
      console.log("total posts: ", this.numberOfPosts);
    });
    this.bulletinBoardService
      .getNumberOfCreatedPosts()
      .subscribe((res: any) => {
        this.numberOfCreatedPosts = res;
        console.log("created posts: ", this.numberOfCreatedPosts);
      });
    this.environmentService.createLoader(() => this.fetchPosts());

    this.bulletinBoardService.getPostSubscription().subscribe((req) => {
      const postId = req?.postId;
      const method = req?.method;
      if (method == "update") {
        this.onUpdatePost(postId);
      } else if (method == "delete") {
        this.onDeletePost(postId);
      }
    });
  }
  /**
   * remove the new post notification badge
   */
  removePostBadge() {
    this.badgeService.updateNewPostBadge(false);
  }
  /**
   * Event handler for segment change (all post vs. liked post)
   * @param ev
   */
  async segmentChanged(ev) {
    this.viewMode = ev.detail.value;

    if (this.viewMode == "fav-post") {
      this.fav_posts = [];
      this.environmentService.createLoader(() =>
        this.favoriteService.getLikes(this.currentUser).then((likes) => {
          likes.forEach(async (like) => {
            const postId = like.id;
            let post = await this.bulletinBoardService.getPostFromId(postId);
            if (post) this.fav_posts.push(post);
          });
        })
      );
    }
  }
  /**
   * present edit post modal page
   * @returns Promise<void>
   */
  async presentPostEditModal() {
    const modal = await this.modalController.create({
      component: PostEditPage,
      componentProps: {
        isEdit: false,
      },
    });
    modal.onDidDismiss().then(async (data) => {
      let newPostId = data.data.newPostId;
      let newPost = await this.bulletinBoardService.getPostFromId(newPostId);
      this.posts.unshift(newPost);
    });
    return await modal.present();
  }
  /**
   * fetch posts, if lastDoc provided then load the posts beginning from the last post, otherwise load from first post in the collection
   * @param lastDoc
   */
  async fetchPosts(lastDoc?) {
    let posts = await this.bulletinBoardService.getPosts(lastDoc);
    this.lastDoc = posts.docs[posts.docs.length - 1];
    if (!lastDoc) {
      this.posts = [];

      this.bulletinBoardService.setNumberOfPostsSeenByCurrUser(
        this.numberOfCreatedPosts
      );
    }
    posts.forEach((post) => {
      let data: any = post.data();
      this.posts.push(data);
    });
    this.removePostBadge();
  }
  /**
   * Event Handler for Page Refresh
   * @param event
   */
  async doRefresh(event) {
    this.refreshing = true;
    await this.fetchPosts();
    event.target.complete();
    this.refreshing = false;
  }

  async ionViewWillEnter() {
    if (window.history.state.postId) {
      const postId = window.history.state.postId;
      this.onUpdatePost(postId);
    }
  }
  /**
   * Event handler on post updating
   * @param postId
   */
  async onUpdatePost(postId) {
    let post = await this.bulletinBoardService.getPostFromId(postId);
    let index = this.fav_posts.findIndex((post) => post.postId == postId);
    this.fav_posts[index] = post;
    index = this.posts.findIndex((post) => post.postId == postId);
    this.posts[index] = post;
  }
  /**
   * Event handler on post deleting
   * @param postId
   */
  async onDeletePost(postId) {
    let index = this.fav_posts.findIndex((post) => post.postId == postId);
    this.fav_posts.splice(index, 1);
    index = this.posts.findIndex((post) => post.postId == postId);
    this.posts.splice(index, 1);
  }
  /**
   * Event Handler when scrolling to the bottom
   * @param event
   */
  async loadData(event) {
    console.log("loadData");
    await this.fetchPosts(this.lastDoc);
    if (this.posts.length == this.numberOfPosts) {
      event.target.disabled = true;
    }
    event.target.complete();
  }
}
