import { Component, OnInit, ViewChild } from "@angular/core";
import { waitUntil } from "async-wait-until";
import { Observable } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { UserInfoService } from "../../services/userInfo.service";
import { BulletinBoardService } from "../../services/bulletin-board.service";
import { Router } from "@angular/router";
import { IonContent } from "@ionic/angular";
@Component({
  selector: "app-comments",
  templateUrl: "./comments.page.html",
  styleUrls: ["./comments.page.scss"],
})
export class CommentsPage implements OnInit {
  postId: string;
  currentUser: any;
  comments: Observable<any[]>;
  newComment = "";
  @ViewChild("msgContainer") container: IonContent;

  constructor(
    private router: Router,
    private bulletinBoardService: BulletinBoardService,
    public translate: TranslateService,
    private userInfo: UserInfoService
  ) {}

  async ngOnInit() {
    await waitUntil(() => window.history.state.postId != null);
    this.userInfo.getCurrentUser().then((user) => {
      this.currentUser = user;
    });
    this.postId = window.history.state.postId;
    this.comments = this.bulletinBoardService.getComments(this.postId);
  }
  /**
   * add a comment to the post
   */
  async sendComment() {
    await this.bulletinBoardService
      .createComment(this.postId, this.newComment)
      .then(() => {
        this.newComment = "";
      })
      .catch((err) => {
        console.error(err);
      });
    await this.bulletinBoardService.updateNumberOfComment(this.postId);
    this.container.scrollToBottom();
  }
  /**
   * send post id back to bulletin board page to update the corresponding post
   */
  sendPostId() {
    this.router.navigate(["app/bulletin-board"], {
      state: { postId: this.postId },
    });
  }

  ionViewDidEnter() {
    this.container.scrollToBottom();
  }
}
