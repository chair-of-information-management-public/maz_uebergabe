import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommentEditPage } from './comment-edit.page';

describe('CommentEditPage', () => {
  let component: CommentEditPage;
  let fixture: ComponentFixture<CommentEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommentEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
