import { Component, Input, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { UserInfoService } from "../../../services/userInfo.service";
import { BulletinBoardService } from "../../../services/bulletin-board.service";

@Component({
  selector: "app-comment-edit",
  templateUrl: "./comment-edit.page.html",
  styleUrls: ["./comment-edit.page.scss"],
})
export class CommentEditPage implements OnInit {
  @Input() authorId: string;
  @Input() postId: string;
  @Input() commentId: string;
  @Input() content: string;
  userAvatar: any;
  constructor(
    private modalController: ModalController,
    private BulletinBoardService: BulletinBoardService,
    private userInfo: UserInfoService
  ) {}

  async ngOnInit() {
    this.userAvatar = await (
      await this.userInfo.getUserInfoFromId(this.authorId)
    ).photoURL;
  }
  dismissModal() {
    this.modalController.dismiss({
      dismissed: true,
    });
  }
  updateComment() {
    this.BulletinBoardService.updateComment(
      this.authorId,
      this.postId,
      this.commentId,
      this.content
    );
    this.dismissModal();
  }
}
