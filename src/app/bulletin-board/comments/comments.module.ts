import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { CommentsPageRoutingModule } from "./comments-routing.module";
import { TranslateModule } from "@ngx-translate/core";
import { CommentsPage } from "./comments.page";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommentsPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [CommentsPage],
})
export class CommentsPageModule {}
