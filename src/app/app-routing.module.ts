import { NgModule } from "@angular/core";
import { redirectUnauthorizedTo } from "@angular/fire/auth-guard";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AngularFireAuthGuard } from "@angular/fire/auth-guard";

const redirectUnauthorizedToLogin = () =>
  redirectUnauthorizedTo(["/firebase/auth/sign-in"]);
const redirectUnauthorizedToWalkthrough = () =>
  redirectUnauthorizedTo(["/walkthrough"]);

const routes: Routes = [
  {
    path: "",
    redirectTo: "loginCheck",
    pathMatch: "full",
  },
  {
    path: "walkthrough",
    loadChildren: () =>
      import("./walkthrough/walkthrough.module").then(
        (m) => m.WalkthroughPageModule
      ),
  },
  {
    path: "loginCheck",
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToWalkthrough },
    loadChildren: () =>
      import("./firebase/auth/firebase-auth.module").then(
        (m) => m.FirebaseAuthModule
      ),
  },
  {
    path: "auth/login",
    redirectTo: "firebase/auth/sign-in",
    pathMatch: "full",
  },
  {
    path: "app",
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
    loadChildren: () =>
      import("./tabs/tabs.module").then((m) => m.TabsPageModule),
  },
  {
    path: "firebase",
    redirectTo: "firebase/auth/sign-in",
    pathMatch: "full",
  },
  {
    path: "firebase/auth",
    loadChildren: () =>
      import("./firebase/auth/firebase-auth.module").then(
        (m) => m.FirebaseAuthModule
      ),
  },
  {
    path: "categories",
    canActivate: [AngularFireAuthGuard],
    loadChildren: () =>
      import("./infohub/categories/categories.module").then(
        (m) => m.CategoriesPageModule
      ),
  },
  {
    path: "infohub",
    canActivate: [AngularFireAuthGuard],
    loadChildren: () =>
      import("./infohub/infohub.module").then((m) => m.InfohubPageModule),
  },
  {
    path: "openvsnmodal",
    loadChildren: () =>
      import("./infohub/infoticket/openvsnmodal/openvsnmodal.module").then(
        (m) => m.OpenvsnmodalPageModule
      ),
  },
  {
    path: "chat",
    loadChildren: () =>
      import("./chat/chat.module").then((m) => m.ChatPageModule),
  },
  {
    path: "chat-modal",
    loadChildren: () =>
      import("./maps/chat-modal/chat-modal.module").then(
        (m) => m.ChatModalPageModule
      ),
  },
  {
    path: "videomodal",
    loadChildren: () =>
      import("../app/maps/videomodal/videomodal.module").then(
        (m) => m.VideomodalPageModule
      ),
  },
  {
    path: "openvsnmodal",
    loadChildren: () =>
      import("./infohub/infoticket/openvsnmodal/openvsnmodal.module").then(
        (m) => m.OpenvsnmodalPageModule
      ),
  },
  {
    path: "chat",
    loadChildren: () =>
      import("./chat/chat.module").then((m) => m.ChatPageModule),
  },
  {
    path: "direction-form",
    loadChildren: () =>
      import("./maps/direction-form/direction-form.module").then(
        (m) => m.DirectionFormPageModule
      ),
  },
  {
    path: "bulletin-board",
    loadChildren: () =>
      import("./bulletin-board/bulletin-board.module").then(
        (m) => m.BulletinBoardPageModule
      ),
  },
  {
    path: "edit-profile",
    loadChildren: () =>
      import("./sett/edit-profile/edit-profile.module").then(
        (m) => m.EditProfilePageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      paramsInheritanceStrategy: "always",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
