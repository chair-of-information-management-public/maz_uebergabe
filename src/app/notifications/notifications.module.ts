import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';

import { NotificationsPage } from './notifications.page';
import { NotificationsResolver } from '../notifications/notifications.resolver';
import { NotificationsService } from '../notifications/notifications.service';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ComponentsModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: NotificationsPage,
        canActivate: [AngularFireAuthGuard],
        resolve: {
          data: NotificationsResolver
        }
      }
    ])
  ],
  declarations: [ NotificationsPage ],
  providers: [
    NotificationsResolver,
    NotificationsService
  ]
})
export class NotificationsPageModule {}
