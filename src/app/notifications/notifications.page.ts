import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AlertController, ToastController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { Subscription, Observable } from "rxjs";
import { switchMap } from "rxjs/operators";
import { LanguageService } from "../language/language.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-notifications",
  templateUrl: "./notifications.page.html",
  styleUrls: [
    "./styles/notifications.page.scss",
    "./styles/notifications.shell.scss",
  ],
})
export class NotificationsPage implements OnInit {
  // Gather all component subscription in one place. Can be one Subscription or multiple (chained using the Subscription.add() method)
  subscriptions: Subscription;

  notifications: any;
  available_languages = [];
  translations;
  hideMe= true;
  constructor(
    private route: ActivatedRoute,
    public languageService: LanguageService,
    public translate: TranslateService,
    public alertController: AlertController,
    private router: Router,
    public toastController: ToastController
  ) {}

  ngOnInit(): void {
    this.subscriptions = this.route.data
      .pipe(
        // Extract data for this page
        switchMap((resolvedRouteData: { source: Observable<any> }) => {
          return resolvedRouteData["data"].source;
        })
      )
      .subscribe(
        (pageData) => {
          this.notifications = pageData;
        },
        (error) => console.log(error)
      );
  }
  
  
  
  
  sendData(adress: any){
    this.router.navigate(['app/maps/maps'], {state: {example: adress}});
  }
  sendCardData(card: boolean){
    this.hideMe = !this.hideMe;
    this.router.navigate(['app/maps/maps'], {state: {card: card}});
  }    

  // NOTE: Ionic only calls ngOnDestroy if the page was popped (ex: when navigating back)
  // Since ngOnDestroy might not fire when you navigate from the current page, use ionViewWillLeave to cleanup Subscriptions
  ionViewWillLeave(): void {
    this.subscriptions.unsubscribe();
  }
}
