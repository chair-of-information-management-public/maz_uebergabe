import { Component, NgZone, OnDestroy } from "@angular/core";
import { Location } from "@angular/common";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import {
  AlertController,
  LoadingController,
  ModalController,
} from "@ionic/angular";
import { Subscription } from "rxjs";

import { HistoryHelperService } from "../../../utils/history-helper.service";
import { FirebaseAuthService } from "../firebase-auth.service";
import { LanguageService } from "../../../language/language.service";
import { TranslateService } from "@ngx-translate/core";
import { Storage } from "@ionic/storage";
import { MenuController } from "@ionic/angular";
import { DatenschutzPage } from "../../../../../src/app/components/datenschutz/datenschutz.page";

@Component({
  selector: "app-firebase-sign-in",
  templateUrl: "./firebase-sign-in.page.html",
  styleUrls: ["./styles/firebase-sign-in.page.scss"],
})
export class FirebaseSignInPage implements OnDestroy {
  loginForm: FormGroup;
  subscriptions: Subscription;
  submitError: string;
  redirectLoader: HTMLIonLoadingElement;
  authRedirectResult: Subscription;
  available_languages = [];
  translations;
  obj;
  flag;
  name;
  validation_messages;
  translateCheck;

  constructor(
    public router: Router,
    public languageService: LanguageService,
    public route: ActivatedRoute,
    private modalController: ModalController,
    public authService: FirebaseAuthService,
    private ngZone: NgZone,
    public loadingController: LoadingController,
    public location: Location,
    public historyHelper: HistoryHelperService,
    public translate: TranslateService,
    public alertController: AlertController,
    private storage: Storage,
    public menuCtrl: MenuController
  ) {
    this.validation_messages = {
      email: [
        { type: "required", message: this.translate.instant("mailRequired") },
        { type: "pattern", message: this.translate.instant("pattern") },
      ],
      password: [
        {
          type: "required",
          message: this.translate.instant("passwortRequired"),
        },
        {
          type: "minlength",
          message: this.translate.instant("minLength"),
        },
      ],
    };

    this.loginForm = new FormGroup({
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
      password: new FormControl(
        "",
        Validators.compose([Validators.minLength(6), Validators.required])
      ),
    });

    // Get firebase authentication redirect result invoken when using signInWithRedirect()
    // signInWithRedirect() is only used when client is in web but not desktop
    this.authRedirectResult = this.authService
      .getRedirectResult()
      .subscribe((result) => {
        if (result.user) {
          this.redirectLoggedUserToProfilePage();
        } else if (result.error) {
          this.manageAuthWithProvidersErrors(result.error);
        }
      });

    // Check if url contains our custom 'auth-redirect' param, then show a loader while we receive the getRedirectResult notification
    this.route.queryParams.subscribe((params) => {
      const authProvider = params["auth-redirect"];
      if (authProvider) {
        this.presentLoading(authProvider);
      }
    });
  }

  ngOnInit(): void {
    this.menuCtrl.enable(false);
    this.subscriptions = this.route.data.subscribe(
      (state) => {
        // get translations for this page to use in the Language Chooser Alert
        this.getTranslations();
      },
      (error) => console.log(error)
    );

    this.translate.onLangChange.subscribe(() => this.getTranslations());
    this.obj = this.languageService.getFlags(this.translate.currentLang);
    this.flag = this.obj.flag;
    this.name = this.flag.name;
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  ionViewDidLeave() {
    this.menuCtrl.enable(true);
  }

  ngOnDestroy(): void {
    this.dismissLoading();
  }
  async openModal() {
    const modal = await this.modalController.create({
      component: DatenschutzPage,
    });
    return await modal.present();
  }

  // Once the auth provider finished the authentication flow, and the auth redirect completes,
  // hide the loader and redirect the user to the profile page
  redirectLoggedUserToProfilePage() {
    this.dismissLoading();
    // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
    // That's why we need to wrap the router navigation call inside an ngZone wrapper
    this.ngZone.run(() => {
      // Get previous URL from our custom History Helper
      // If there's no previous page, then redirect to profile
      // const previousUrl = this.historyHelper.previousUrl || 'firebase/auth/profile';
      const previousUrl = "app/categories";

      // No need to store in the navigation history the sign-in page with redirect params (it's justa a mandatory mid-step)
      // Navigate to profile and replace current url with profile
      this.router.navigateByUrl("app/categories");
    });
  }

  async presentLoading(authProvider?: string) {
    const authProviderCapitalized =
      authProvider[0].toUpperCase() + authProvider.slice(1);

    this.loadingController
      .create({
        message: authProvider
          ? "Signing in with " + authProviderCapitalized
          : "Signin in ...",
      })
      .then((loader) => {
        const currentUrl = this.router.url;
        if (currentUrl.includes("/firebase/auth/sign-in")) {
          this.redirectLoader = loader;
          this.redirectLoader.present();
        }
      });
  }

  async dismissLoading() {
    if (this.redirectLoader) {
      await this.redirectLoader.dismiss();
    }
  }

  // Before invoking auth provider redirect flow, present a loading indicator and add a flag to the path.
  // The precense of the flag in the path indicates we should wait for the auth redirect to complete.
  prepareForAuthWithProvidersRedirection(authProvider: string) {
    this.presentLoading(authProvider);

    this.location.replaceState(
      this.location.path(),
      "auth-redirect=" + authProvider,
      this.location.getState()
    );
  }

  manageAuthWithProvidersErrors(errorMessage: string) {
    this.submitError = errorMessage;
    // remove auth-redirect param from url
    this.location.replaceState(this.router.url.split("?")[0], "");
    this.dismissLoading();
  }

  resetSubmitError() {
    this.submitError = null;
  }

  signInWithEmail() {
    this.resetSubmitError();
    this.authService
      .signInWithEmail(
        this.loginForm.value["email"],
        this.loginForm.value["password"]
      )
      .then((user) => {
        // navigate to user profile
        this.redirectLoggedUserToProfilePage();
      })
      .catch((error) => {
        this.submitError = error.message;
        this.dismissLoading();
      });
  }

  async openLanguageChooser() {
    this.available_languages = this.languageService
      .getLanguages()
      .map((item) => ({
        name: item.name,
        type: "radio",
        label: item.name,
        value: item.code,
        flag: item.flag,
        checked: item.code === this.translate.currentLang,
      }));

    const alert = await this.alertController.create({
      header: this.translations.SELECT_LANGUAGE,
      inputs: this.available_languages,
      cssClass: "language-alert",
      buttons: [
        {
          text: this.translations.CANCEL,
          role: "cancel",
          cssClass: "secondary",
          handler: () => {},
        },
        {
          text: this.translations.OK,
          handler: async (data) => {
            if (data) {
              await this.translate.use(data);
              await this.storage.set("language", data);
              this.translate.setDefaultLang(data);
              this.translate.currentLang = data;
              const mailRequired = this.translate.instant("mailRequired");
              const passwortRequired =
                this.translate.instant("passwortRequired");
              const pattern = this.translate.instant("pattern");
              const minLength = this.translate.instant("minLength");
              this.validation_messages = {
                email: [
                  { type: "required", message: mailRequired },
                  {
                    type: "pattern",
                    message: pattern,
                  },
                ],
                password: [
                  { type: "required", message: passwortRequired },
                  {
                    type: "minlength",
                    message: minLength,
                  },
                ],
              };
            }
            this.obj = await this.languageService.getFlags(
              this.translate.currentLang
            );
            this.flag = this.obj.flag;
            this.name = this.flag.name;
            this.translate.currentLang = data;
            window.location.reload();
          },
        },
      ],
    });
    await alert.present();
    var radios = document.getElementsByClassName("alert-radio-label");
    for (let index = 0; index < radios.length; index++) {
      let singrad = radios[index];
      singrad.innerHTML = singrad.innerHTML.concat(
        "<img src=" +
          this.available_languages[index].flag +
          ' style="width:30px; position:absolute; right:10px;"/>'
      );
    }
  }
  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(this.translate.currentLang)
      .subscribe((translations) => (this.translations = translations));
  }

  async openResetPasswordForm() {
    const alert = await this.alertController.create({
      header:
        "Please enter your email address, so we can send you an email to reset your password.", //translate
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "alert-button-cancel",
        },
        {
          text: "Send Password Reset E-Mail", //translate
          cssClass: "alert-button-confirm",
          handler: (data) => {
            this.authService.sendPasswordResetEmail(data.email);
          },
        },
      ],
      inputs: [
        {
          name: "email",
          type: "email",
          placeholder: "E-mail",
        },
      ],
    });

    await alert.present();
  }
}
