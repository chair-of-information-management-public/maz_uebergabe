import { Component, OnInit, NgZone } from "@angular/core";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { Location } from "@angular/common";
import { Router, ActivatedRoute } from "@angular/router";
import {
  MenuController,
  LoadingController,
  ModalController,
} from "@ionic/angular";
import { PasswordValidator } from "../../../validators/password.validator";
import { FirebaseAuthService } from "../firebase-auth.service";
import { Subscription } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { DatenschutzPage } from "../../../../../src/app/components/datenschutz/datenschutz.page";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: "app-firebase-sign-up",
  templateUrl: "./firebase-sign-up.page.html",
  styleUrls: ["./styles/firebase-sign-up.page.scss"],
})
export class FirebaseSignUpPage implements OnInit {
  signupForm: FormGroup;
  matching_passwords_group: FormGroup;
  submitError: string;
  redirectLoader: HTMLIonLoadingElement;
  authRedirectResult: Subscription;
  subscriptions: Subscription;
  translations;
  validation_messages;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public menu: MenuController,
    public authService: FirebaseAuthService,
    private ngZone: NgZone,
    public translate: TranslateService,
    public loadingController: LoadingController,
    public location: Location,
    public menuCtrl: MenuController,
    private modalController: ModalController,
    private afs: AngularFirestore
  ) {
    const mailRequired = this.translate.instant("mailRequired");
    const passwortRequired = this.translate.instant("passwortRequired");
    const pattern = this.translate.instant("pattern");
    const patternUser = this.translate.instant("patternUser");
    const userRequired = this.translate.instant("userRequired");
    const minLength = this.translate.instant("minLength");
    const notequal = this.translate.instant("notEqual");
    const confirm = this.translate.instant("confirm");
    this.validation_messages = {
      email: [
        { type: "required", message: mailRequired },
        { type: "pattern", message: pattern },
      ],
      user: [
        { type: "required", message: userRequired },
        { type: "pattern", message: patternUser },
      ],
      password: [
        { type: "required", message: passwortRequired },
        {
          type: "minlength",
          message: minLength,
        },
      ],
      confirm_password: [
        {
          type: "required",
          message: confirm,
        },
      ],
      matching_passwords: [{ type: "areNotEqual", message: notequal }],
    };
    this.matching_passwords_group = new FormGroup(
      {
        password: new FormControl(
          "",
          Validators.compose([Validators.minLength(6), Validators.required])
        ),
        confirm_password: new FormControl("", Validators.required),
      },
      (formGroup: FormGroup) => {
        return PasswordValidator.areNotEqual(formGroup);
      }
    );

    this.signupForm = new FormGroup({
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
      user: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+$"),
        ])
      ),
      dataPolicyConfirm: new FormControl(false, Validators.pattern("true")),
      matching_passwords: this.matching_passwords_group,
    });
    // Get firebase authentication redirect result invoken when using signInWithRedirect()
    // signInWithRedirect() is only used when client is in web but not desktop
    this.authRedirectResult = this.authService
      .getRedirectResult()
      .subscribe((result) => {
        if (result.user) {
          this.redirectLoggedUserToProfilePage();
        } else if (result.error) {
          this.manageAuthWithProvidersErrors(result.error);
        }
      });

    // Check if url contains our custom 'auth-redirect' param, then show a loader while we receive the getRedirectResult notification
    this.route.queryParams.subscribe((params) => {
      const authProvider = params["auth-redirect"];
      if (authProvider) {
        this.presentLoading(authProvider);
      }
    });
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: DatenschutzPage,
    });
    return await modal.present();
  }
  ngOnInit(): void {
    this.menu.enable(false);
    this.subscriptions = this.route.data.subscribe(
      (state) => {
        // get translations for this page to use in the Language Chooser Alert
        this.getTranslations();
      },
      (error) => console.log(error)
    );

    this.translate.onLangChange.subscribe(() => this.getTranslations());
  }

  // Once the auth provider finished the authentication flow, and the auth redirect completes,
  // hide the loader and redirect the user to the profile page
  redirectLoggedUserToProfilePage() {
    this.dismissLoading();

    // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
    // That's why we need to wrap the router navigation call inside an ngZone wrapper
    this.ngZone.run(() => {
      // Get previous URL from our custom History Helper
      // If there's no previous page, then redirect to profile
      // const previousUrl = this.historyHelper.previousUrl || 'firebase/auth/profile';
      const previousUrl = "app/categories";

      // No need to store in the navigation history the sign-in page with redirect params (it's justa a mandatory mid-step)
      // Navigate to profile and replace current url with profile
      this.router.navigate([previousUrl], { replaceUrl: true });
    });
  }

  async presentLoading(authProvider?: string) {
    const authProviderCapitalized =
      authProvider[0].toUpperCase() + authProvider.slice(1);
    this.redirectLoader = await this.loadingController.create({
      message: authProvider
        ? "Signing up with " + authProviderCapitalized
        : "Signin up ...",
    });
    await this.redirectLoader.present();
  }

  async dismissLoading() {
    if (this.redirectLoader) {
      await this.redirectLoader.dismiss();
    }
  }

  resetSubmitError() {
    this.submitError = null;
  }

  // Before invoking auth provider redirect flow, present a loading indicator and add a flag to the path.
  // The precense of the flag in the path indicates we should wait for the auth redirect to complete.
  prepareForAuthWithProvidersRedirection(authProvider: string) {
    this.presentLoading(authProvider);

    this.location.go(
      this.location.path(),
      "auth-redirect=" + authProvider,
      this.location.getState()
    );
  }

  manageAuthWithProvidersErrors(errorMessage: string) {
    this.submitError = errorMessage;
    // remove auth-redirect param from url
    this.location.replaceState(this.router.url.split("?")[0], "");
    this.dismissLoading();
  }

  signUpWithEmail(): void {
    const values = this.signupForm.value;
    // if (values.email != "") {
    this.resetSubmitError();
    this.authService
      .signUpWithEmail(
        values.email,
        values.matching_passwords.password,
        values.user
      )
      .then(() => {
        // navigate to log in page
        this.router.navigate(["/firebase/auth/sign-in"]);
      })
      .catch((error) => {
        this.submitError = error.message;
      });
  }
  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(this.translate.currentLang)
      .subscribe((translations) => (this.translations = translations));
  }
  ionViewDidLeave() {
    this.menuCtrl.enable(true);
  }
}
