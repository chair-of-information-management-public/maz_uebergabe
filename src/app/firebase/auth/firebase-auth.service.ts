import { Inject, Injectable, PLATFORM_ID } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Observable, Subject, from, of } from "rxjs";
import { DataStore } from "../../shell/data-store";
import { Platform } from "@ionic/angular";
import { filter, map } from "rxjs/operators";

import { User, auth } from "firebase/app";
import { cfaSignIn, cfaSignOut } from "capacitor-firebase-auth";
import { isPlatformBrowser } from "@angular/common";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable()
export class FirebaseAuthService {
  currentUser: any;
  userProviderAdditionalInfo: any;
  redirectResult: Subject<any> = new Subject<any>();

  constructor(
    public angularFire: AngularFireAuth,
    public platform: Platform,
    @Inject(PLATFORM_ID) private platformId: object,
    private afs: AngularFirestore
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.angularFire.onAuthStateChanged((user) => {
        if (user) {
          // User is signed in.
          this.currentUser = user;
        } else {
          // No user is signed in.
          this.currentUser = null;
        }
      });

      if (!this.platform.is("capacitor")) {
        // when using signInWithRedirect, this listens for the redirect results
        this.angularFire.getRedirectResult().then(
          (result) => {
            // result.credential.accessToken gives you the Provider Access Token. You can use it to access the Provider API.
            if (result.user) {
              this.userProviderAdditionalInfo =
                result.additionalUserInfo.profile;
              this.redirectResult.next(result);
            }
          },
          (error) => {
            this.redirectResult.next({ error: error.code });
          }
        );
      }
    }
  }

  getRedirectResult(): Observable<any> {
    return this.redirectResult.asObservable();
  }

  getPhotoURL(signInProviderId: string, photoURL: string): string {
    // Default imgs are too small and our app needs a bigger image
    switch (signInProviderId) {
      case "facebook.com":
        return photoURL + "?height=400";
      case "password":
        return "https://s3-us-west-2.amazonaws.com/ionicthemes/otros/avatar-placeholder.png";
      case "twitter.com":
        return photoURL.replace("_normal", "_400x400");
      case "google.com":
        return photoURL.split("=")[0];
      default:
        return photoURL;
    }
  }

  signOut(): Observable<any> {
    if (this.platform.is("capacitor")) {
      return cfaSignOut();
    } else {
      return from(this.angularFire.signOut());
    }
  }

  signInWithEmail(email: string, password: string): Promise<any> {
    return this.angularFire
      .signInWithEmailAndPassword(email, password)
      .then((credential) => {
        if (credential.user.emailVerified !== true) {
          this.signOut();
          this.SendVerificationMail();
        } else {
          const uid = credential.user.uid;
          // update verification status
          this.afs.doc(`normalUser/${uid}`).update({
            emailVerified: credential.user.emailVerified,
          });
        }
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

  signUpWithEmail(
    email: string,
    password: string,
    username: string
  ): Promise<any> {
    return this.angularFire
      .createUserWithEmailAndPassword(email, password)
      .then((credential) => {
        credential.user.updateProfile({
          displayName: username,
          photoURL: "../../../../assets/icon/avatar_default.png",
        });
        const uid = credential.user.uid;
        // store user data in database
        this.afs.doc(`normalUser/${uid}`).set({
          uid,
          email: credential.user.email,
          username: username,
          avatar: "../../../../assets/icon/avatar_default.png",
          emailVerified: credential.user.emailVerified,
        });
        this.SendVerificationMail(); // Sending email verification notification, when new user registers
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

  socialSignIn(providerName: string, scopes?: Array<string>): Observable<any> {
    if (this.platform.is("capacitor")) {
      return cfaSignIn(providerName);
    } else {
      const provider = new auth.OAuthProvider(providerName);

      if (scopes) {
        scopes.forEach((scope) => {
          provider.addScope(scope);
        });
      }

      if (this.platform.is("desktop")) {
        return from(this.angularFire.signInWithPopup(provider));
      } else {
        // web but not desktop, for example mobile PWA
        return from(this.angularFire.signInWithRedirect(provider));
      }
    }
  }

  signInWithFacebook() {
    const provider = new auth.FacebookAuthProvider();
    return this.socialSignIn(provider.providerId);
  }

  signInWithGoogle() {
    const provider = new auth.GoogleAuthProvider();
    const scopes = ["profile", "email"];
    return this.socialSignIn(provider.providerId, scopes);
  }

  signInWithTwitter() {
    const provider = new auth.TwitterAuthProvider();
    return this.socialSignIn(provider.providerId);
  }

  SendVerificationMail() {
    return this.angularFire.currentUser
      .then((user) => {
        return user.sendEmailVerification();
      })
      .then(() => {
        window.alert(
          "Verification E-mail was sent to your E-mail address. Please kindly check the inbox (including your Spam folder) and verify your E-mail." //translate
        );
      });
  }
  sendPasswordResetEmail(email) {
    return this.angularFire.sendPasswordResetEmail(email);
  }
  deleteUser(currentUser){
    currentUser.delete();
  }
}
