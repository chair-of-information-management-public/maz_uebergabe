import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ContactListPageRoutingModule } from "./contact-list-routing.module";

import { ContactListPage } from "./contact-list.page";
import { TranslateModule } from "@ngx-translate/core";
import { ComponentsModule } from "../../components/components.module";
import { IonicSelectableModule } from "ionic-selectable";

@NgModule({
  imports: [
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ContactListPageRoutingModule,
    TranslateModule,
    IonicSelectableModule,
  ],
  declarations: [ContactListPage],
})
export class ContactListPageModule {}
