import { Component, OnInit, OnDestroy } from "@angular/core";
import { switchMap } from "rxjs/operators";
import { LanguageService } from "../../language/language.service";
import { TranslateService } from "@ngx-translate/core";
import { ChatService } from "../../services/chat.service";
import { User, UserInfoService } from "../../services/userInfo.service";
import { Observable, Subscription } from "rxjs";
import { Router } from "@angular/router";
import { IonicSelectableComponent } from "ionic-selectable";
import { ViewChild } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollectionGroup,
  CollectionReference,
} from "@angular/fire/firestore";
import { Environment } from "../../services/environment.service";

@Component({
  selector: "app-contact-list",
  templateUrl: "./contact-list.page.html",
  styleUrls: ["./contact-list.page.scss"],
})
export class ContactListPage implements OnInit {
  existingChats: Observable<any[]>;
  chatSubscription: Subscription;
  allUsers = [];
  currentUser;
  userRole: any;
  overallNewMessages: any;
  selected_page = null;

  @ViewChild("contactList") contactList: IonicSelectableComponent;

  constructor(
    private languageService: LanguageService,
    private chatService: ChatService,
    private router: Router,
    private afs: AngularFirestore,
    private translate: TranslateService,
    private userInfo: UserInfoService,
    private environmentService: Environment
  ) {}

  async ngOnInit() {
    // get current user ID
    this.currentUser = await this.userInfo.getCurrentUser();

    // get a list of all users
    this.userInfo
      .getUserRole()
      .then((res) => {
        this.userInfo.setUserRole(res);
        this.userRole = res;
      })
      .catch((err) => console.log(err))
      .finally(() => {
        this.userInfo.getUsers().subscribe((userArr) => {
          //get users with existing chats
          this.existingChats = this.chatService.getExistingChats();
          this.environmentService.createLoader(() => {
            return new Promise((res) => {
              if (this.existingChats !== null) res();
            });
          });
          this.allUsers = userArr;
          this.chatSubscription = this.existingChats.subscribe((chats) => {
            //get number of new messages
            chats.forEach((chatObj) => {
              let chat = chatObj.chat;
              this.afs
                .collection("chats")
                .doc(chat.id)
                .collection("newMessages")
                .doc(this.currentUser)
                .valueChanges()
                .subscribe((doc) => {
                  chat.newMessages = doc.newMessages;
                });
              this.environmentService.dismissLoader();
              //get timestamp of last message
              this.afs
                .collection("chats")
                .doc(chat.id)
                .collection("messages", (ref) => ref.orderBy("createdAt"))
                .valueChanges({ idField: "id" })
                .subscribe((messages) => {
                  let lastMessage = messages[messages.length - 1].id;
                  this.afs
                    .collection("chats")
                    .doc(chat.id)
                    .collection("messages")
                    .doc(lastMessage)
                    .valueChanges()
                    .subscribe((documentData) => {
                      chat.lastMessageTimestamp = documentData.createdAt;
                    });
                });
            });
          });
        });
      });
  }

  contactChange(event) {
    // let uid = event.item.uid;
    // console.log(`${uid} was chosen`);
    console.log("on close: ", event);
    // this.router.navigate(["/app/chat/" + uid]);
  }

  onClose($event) {
    console.log("on close: ", this.selected_page);
    this.router.navigate([
      "/app/chat",
      this.selected_page.uid,
      this.selected_page["name_" + this.translate.currentLang],
      this.selected_page.icon,
    ]);
  }
  ngOnDestroy(): void {
    this.chatSubscription.unsubscribe();
  }
}
