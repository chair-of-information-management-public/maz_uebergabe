import { Component, OnInit, ViewChild } from "@angular/core";
import { IonContent, Platform } from "@ionic/angular";
import { Observable } from "rxjs";
import { ChatService } from "../services/chat.service";
import { Router, ActivatedRoute } from "@angular/router";
import { LanguageService } from "../language/language.service";
import { TranslateService } from "@ngx-translate/core";
import { UserInfoService } from "../services/userInfo.service";
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.page.html",
  styleUrls: ["./chat.page.scss"],
})
export class ChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  messages: Observable<any[]>;
  newMsg = ""; //correspondent
  uid = "";
  bottomReached = true;
  correspondentName;
  correspondentAvatar;
  chatId;

  constructor(
    private chatService: ChatService,
    private router: Router,
    public languageService: LanguageService,
    private route: ActivatedRoute,
    private platform: Platform,
    private userInfoService: UserInfoService,
    private afs: AngularFirestore,
    private translate: TranslateService
  ) {}

  async ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.uid = params.get("uid");
      this.correspondentName = params.get("username");
      this.correspondentAvatar = params.get("avatar");
    });
    await this.chatService.setChatID(this.uid).then((res) => {
      this.chatId = res;
    });

    this.messages = this.chatService.getChatMessages(this.chatId);
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 50);
  }
  /**
   * send user message
   */
  sendMessage() {
    this.chatService.addChatMessage(this.newMsg, this.uid).then(() => {
      this.newMsg = "";
      setTimeout(() => {
        this.content.scrollToBottom();
      }, 100);
    });
  }
  /**
   * Even handler for scrolling chat page
   * @param $event
   */
  async scrollHandling($event) {
    let pos = $event.detail.scrollTop + 5;
    const scrollElement = await $event.target.getScrollElement();
    const scrollHeight =
      scrollElement.scrollHeight - scrollElement.clientHeight;
    if (pos < scrollHeight) this.bottomReached = false;
    else this.bottomReached = true;
  }

  ionViewWillLeave() {
    this.chatService.markAsRead();
  }
}
