import {
  Component,
  ViewChild,
  AfterViewInit,
  ElementRef,
  ChangeDetectorRef,
  NgZone,
} from "@angular/core";
import {
  AlertController,
  LoadingController,
  ModalController,
  NavController,
} from "@ionic/angular";

import { GoogleMapComponent } from "../../components/google-map/google-map.component";
import { LanguageService } from "../../language/language.service";
import { TranslateService } from "@ngx-translate/core";
import { AlertService } from "../../services/alert.service";
import { MapsService } from "../../services/maps.service";
import { waitUntil } from "async-wait-until";
import { Storage } from "@ionic/storage";
import { UserInfoService } from "../../services/userInfo.service";
import { FavoritesService } from "../../services/favorites.service";
import { RouteCalculationService } from "../../services/route-calculation.service";
import { LocalNotificationsService } from "../../services/local-notifications.service";
import { from, Observable, Subscription } from "rxjs";
import { filter, map, switchMap } from "rxjs/operators";
import { ActivatedRoute, NavigationStart, Router } from "@angular/router";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { start } from "repl";
import { create } from "core-js/fn/object";
import { IonPullUpFooterState } from "ionic-pullup";
import { AngularFirestore } from "@angular/fire/firestore";
import { BadgeService } from "../../services/badge.service";
import { LogoService } from "../../services/logo.service";
import { Environment } from "../../services/environment.service";
import { ChatModalPage } from "../chat-modal/chat-modal.page";
import { DirectionFormPage } from "../direction-form/direction-form.page";
import { VideomodalPage } from "../videomodal/videomodal.page";
import {
  VgApiService,
  VgMediaDirective,
} from "@videogular/ngx-videogular/core";
import { ChatService } from "../../services/chat.service";
import { IonicSelectableComponent } from "ionic-selectable";
import { hasUncaughtExceptionCaptureCallback } from "process";
import { log } from "console";
import { LocationService } from "../../services/location.service";
import { Capacitor, Plugins } from "@capacitor/core";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";
import { Platform } from "@ionic/angular";

const { Geolocation, Toast } = Plugins;
@Component({
  selector: "app-maps",
  templateUrl: "./maps.page.html",
  styleUrls: ["./styles/maps.page.scss"],
})
export class MapsPage implements AfterViewInit {
  @ViewChild(GoogleMapComponent, { static: false })
  _GoogleMap: GoogleMapComponent;
  map: google.maps.Map;
  selected_page = null; 
  pages = [];
  @ViewChild(VgMediaDirective, { static: true }) media: VgMediaDirective;
  api: VgApiService;
  urlVideo: string = "";
  items = [
    {
      title: "External file",
      url: "../../assets/videos/Zusammenschnitt final.mp4",
      imagePreview: "assets/MAZ-Ticket.png",
    },
  ];
  pagesSelect = [];
  mapOptions: google.maps.MapOptions = {
    zoom: 15,
    center: { lat: 51.7002, lng: 9.9959 },
    mapTypeControl: false,
    fullscreenControl: false,
    zoomControl: false,
    keyboardShortcuts: false,
    streetViewControl: false,
    // uncomment the following line if you want to remove the default Map controls
    // disableDefaultUI: true
  };
  @ViewChild("directionsPanel") directionsPanel: ElementRef;
  @ViewChild("inputDestination") inputDestination: IonicSelectableComponent;
  @ViewChild("predictions") predictions: IonicSelectableComponent;

  start: any;
  destination: any;
  loadingElement: any;
  notifications: any;
  customPickerOptions: any;
  available_languages = [];
  translations;
  loaderdismissed = false;
  isVideo = false;
  currentUser: any;
  positionUpdated = false;
  pagesSelectReady = false;
  available_POIS = [
    "FahrradverleihGründel",
    "eCarsharingSchlarpe",
    "BürgerfahrenBürger",
    "BürgerbusBodenfelde",
  ];
  current_location: any;
  geocoded_current_location: any;
  id = "navigate";
  infoID = "info";
  poiPosition: any;
  adress: string;
  loading;
  originRoute: any;
  myDate = new Date().toISOString();
  myMapsDate = new Date();
  destinationRoute: any;
  intervalID: any;
  kmlDisplayNavigation = false;
  markerLive: google.maps.Marker;
  kmlDisplayInformation = false;
  kmlDisplayElse = false;
  kmlDisplayFreizeit = false;
  kmlDisplayMitfahrbaenke = false;
  // isDisplay = true;
  dataIsFetched = false;
  disableButton = false;
  mapsResponse: any;
  subscriptions: Subscription;
  route: ActivatedRoute;
  integration: string;
  mobility: string;
  freetime: string;
  behoerde: string;
  integrationColor: string;
  mobilityColor: string;
  isTransit = false; //check if a step has a transit
  showDirectionPanel = false; //Direction Panel is not shown by default
  mode = "TRANSIT";
  //pullup min and max height
  toolbarTopMargin = 0.45 * screen.height;
  minBottomVisible = 0.1 * screen.height;
  freetimeColor: string;
  behoerdeColor: string;
  linkToAgency: any;
  infowindow = new google.maps.InfoWindow();
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  directionForm: FormGroup;
  publicCurrentLocation: any;
  state$: Observable<object>;
  data: any;
  isDeparture = true; //arrival=false
  userRole: any;
  svgMarker = {
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: "#4285F4",
    fillOpacity: 0.8,
    strokeColor: "white",
    strokeOpacity: 0.7,
    strokeWeight: 4,
    rotation: 0,
    scale: 10,
  };

  // locale / firebase verfügbar machen
  georsslayerNavigation = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/Flexible%20Mobilit%C3%A4t%20(3).kmz?raw=true",
    suppressInfoWindows: true,
  });
  georsslayerMitfahrbaenke = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/Mitfahrb%C3%A4nke%20im%20Harzweserland.kmz?raw=true",
    suppressInfoWindows: true,
  });
  georsslayerFreizeit = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/Freizeit.kmz?raw=true",
    suppressInfoWindows: true,
  });
  georsslayerInformation = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/Beh%C3%B6rde%20und%20Beratung%20(3).kmz?raw=true",
    suppressInfoWindows: true,
  });
  georsslayerElse = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/Sprachkurse%20und%20Integration%20(3).kmz?raw=true",
    suppressInfoWindows: true,
  });
  BürgerbusBodenfelde = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/B%C3%BCrgerbus%20Bodenfelde.kmz?raw=true",
    suppressInfoWindows: true,
    preserveViewport: true,
  });

  BürgerfahrenBürger = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/B%C3%BCrger%20fahren%20B%C3%BCrger.kmz?raw=true",
    suppressInfoWindows: true,
    preserveViewport: true,
  });
  eCarsharingSchlarpe = new google.maps.KmlLayer({
    url: "https://github.com/TillDies/kmldata/blob/main/e-Carsharing%20Schlarpe.kmz?raw=true",
    suppressInfoWindows: true,
    preserveViewport: true,
  });

  footerState: IonPullUpFooterState;
  otherResults: any[]; //outsourced to direction form modal
  selected_dest: any;
  disableChatButton: boolean;
  curLoc: {
    category: string;
    category_label: any;
    name: any;
    address: any;
    image: string;
  };
  watchId: string;
  firstTimeGetLocation: any;

  constructor(
    private loadingController: LoadingController,
    public languageService: LanguageService,
    public mapsService: MapsService,
    public userInfoService: UserInfoService,
    public translate: TranslateService,
    public favoritesService: FavoritesService,
    public routeCalculationService: RouteCalculationService,
    public notificationsService: LocalNotificationsService,
    public alertController: AlertController,
    private fb: FormBuilder,
    private alertService: AlertService,
    public navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    private router: Router,
    private ref: ChangeDetectorRef,
    private db: AngularFirestore,
    private storage: Storage,
    public badgeService: BadgeService,
    public logoService: LogoService,
    private environmentService: Environment,
    private modalCtrl: ModalController,
    private chatService: ChatService,
    private formBuilder: FormBuilder,
    private locationService: LocationService,
    public ngZone: NgZone,
    private diagnostic: Diagnostic,
    private platform: Platform
  ) {
    this.createDirectionForm();
    window.history.state.adress;
    this.firstTimeGetLocation = true;
  }

  //outsourced to direction form modal
  onSearchFail(event: { component: IonicSelectableComponent; text: string }) {
    this.otherResults = [];
    let searchText = event.text;
    var sessionToken = new google.maps.places.AutocompleteSessionToken();
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getPlacePredictions(
      {
        input: searchText,
        sessionToken: sessionToken,
      },
      (res) => {
        if (res.length === 0) {
          return;
        }
        res.forEach((item) => {
          this.otherResults.push({ address: item.description });
        });
      }
    );
  }
  radioGroupChange($event) {
    this.selected_page = {
      name: $event.detail.value,
      address: $event.detail.value,
    };
    this.inputDestination.close();
  }

  //   // Show form.
  //   event.component.showAddItemTemplate();
  // }
  // onSearchSuccess(event: {
  //   component: IonicSelectableComponent;
  //   text: string;
  // }) {
  //   // Hide form.
  //   event.component.hideAddItemTemplate();
  // }

  // timeModeChanged() {
  //   this.isDeparture = !this.isDeparture;
  // }
  // dateChanged(date) {
  //   var myHour = this.myDate.split(":")[0];
  //   var myMinute = this.myDate.split(":")[1];
  //   myHour = myHour.split("T")[1];
  //   var myFullYear = this.myDate.split("T")[0];
  //   var myYear = this.convertStringToNumber(myFullYear.split("-")[0]);
  //   var myMonth = this.convertStringToNumber(myFullYear.split("-")[1]);
  //   var myDay = this.convertStringToNumber(myFullYear.split("-")[2]);
  //   var myHourN = this.convertStringToNumber(myHour);
  //   var myMinuteN = this.convertStringToNumber(myMinute);
  //   this.myMapsDate.setMinutes(myMinuteN);
  //   this.myMapsDate.setHours(myHourN);
  //   this.myMapsDate.setFullYear(myYear, myMonth - 1, myDay);
  // }

  /**
   * Creates a Direction Form with Start and Destination
   */
  createDirectionForm() {
    //Form for user to fill

    this.directionForm = this.fb.group({
      start: ["", Validators.required],
      destination: ["", Validators.required],
    });
  }

  /**
   * Updates the State of the Footer
   */
  toggleFooter() {
    this.footerState =
      this.footerState == IonPullUpFooterState.Collapsed
        ? IonPullUpFooterState.Expanded
        : IonPullUpFooterState.Collapsed;
  }

  async presentModalVideo() {
    const modal = await this.modalCtrl.create({
      component: VideomodalPage,
    });
    return await modal.present();
  }
  //open direction form on click
  async openDirectionForm() {
    const modal = await this.modalCtrl.create({
      component: DirectionFormPage,
      componentProps: {
        pagesSelect: this.pagesSelect,
        otherResults: this.otherResults,
        onSearchFail: this.onSearchFail,
      },
    });
    return await modal.present();
  }
  /**
   * Updates the time mode of route calculation (i.e., departure or arrival at)
   * Outsourced to direction form modal
   */
  // timeModeChanged() {
  //   this.isDeparture = !this.isDeparture;
  // }

  /**
   * Converts the Date of the Form (String) to a Date
   * Outsourced to direction form modal
   */
  // dateChanged() {
  //   [this.myDate, this.myMapsDate] = this.mapsService.stringToDate(
  //     this.myDate,
  //     this.myMapsDate
  //   );
  // }
  /**
   * present the Chat on click
   */
  async presentChatModal() {
    const modal = await this.modalCtrl.create({
      component: ChatModalPage,
    });
    // this.environmentService.setSelectedPage(this.selected_page);
    modal.onDidDismiss().then((data) => {
      if (data.data.routeConfirmed) {
        //add map response to document of normal user
        if (this.userRole === "NORMALUSER") {
          let start,
            destination = this.mapsResponse.request.destination.query,
            travelmode = this.mapsResponse.request.travelMode,
            departureTime =
              this.mapsResponse.routes[0].legs[0].departure_time.value,
            arrivalTime =
              this.mapsResponse.routes[0].legs[0].arrival_time.value,
            createdTime = new Date(),
            mapsResponse = this.mapsResponse.routes[0].overview_polyline,
            normalUserId = this.currentUser,
            initiativeId = this.selected_dest.uid ? this.selected_dest.uid : "",
            linkToAgency = this.linkToAgency;
          if (this.mapsResponse.request.origin.query) {
            start = this.mapsResponse.request.origin.query;
          } else {
            start = this.current_location.toString();
          }
          this.db
            .collection("normalUser")
            .doc(this.currentUser)
            .collection("myRoutes")
            .add({
              start,
              destination,
              travelmode,
              departureTime,
              arrivalTime,
              createdTime,
              mapsResponse,
              normalUserId,
              initiativeId,
              linkToAgency,
            });
        }
      }
    });
    return await modal.present();
  }

  /**
   * remove current calculated Direction
   */
  removeDirection() {
    this.directionsDisplay.setMap(null);
    this.showDirectionPanel = false; 
    this.directionForm.patchValue({ start: "", destination: "" }); 
  }

  /**
   * show playable Ticket-Video
   */
  displayVideo() {
    // show/hide video on Frontend
    this.isVideo = !this.isVideo;
  }

  onPlayerReady(api: VgApiService) {
    this.api = api;
    this.urlVideo = this.items[0].url;
  }

  /**
   * show or hide clicked POI Category
   */
  toggleKML(display: any) {
    this["kmlDisplay" + display] = !this["kmlDisplay" + display];
    if (this["kmlDisplay" + display]) {
      this["georsslayer" + display].setMap(this.map);
    } else {
      this["georsslayer" + display].setMap(null);
    }
  }
  /**
   * Show radius of the respective car sharing provider
   */
  showRadius(kmlName) {
    this[kmlName].setMap(this.map);
  }
  /**
   * Remove radius of car sharing providers
   */
  removeRadius() {
    this.BürgerbusBodenfelde.setMap(null);
    this.BürgerfahrenBürger.setMap(null);
    this.eCarsharingSchlarpe.setMap(null);
  }

  /**
   * get current location and pan to it on first site view
   * update it via watchPosition
   */
  async getMyLocation() {
    const hasPermission = await this.locationService.checkGPSPermission();
    if (hasPermission) {
      if (Capacitor.isNative) {
        const canUseGPS = await this.locationService.askToTurnOnGPS();
        this.postGPSPermission(canUseGPS);
      } else {
        this.postGPSPermission(true);
      }
    } else {
      const permission = await this.locationService.requestGPSPermission();
      if (permission === "CAN_REQUEST" || permission === "GOT_PERMISSION") {
        if (Capacitor.isNative) {
          const canUseGPS = await this.locationService.askToTurnOnGPS();
          this.postGPSPermission(canUseGPS);
        } else {
          this.postGPSPermission(true);
        }
      } else {
        await Toast.show({
          text: "User denied location permission",
        });
      }
    }
  }

  async postGPSPermission(canUseGPS: boolean) {
    if (canUseGPS) {
      this.watchPosition();
    } else {
      await Toast.show({
        text: "Please turn on GPS to get location",
      });
    }
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: this.translate.instant("GPSdeactivated"),
      message: this.translate.instant("GPSOn"),
      buttons: [
        {
          text: this.translate.instant("CANCEL"),
          role: "cancel",
        },
        {
          text: this.translate.instant("OK"),
          handler: () => {
            this.diagnostic.switchToSettings();
          },
        },
      ],
    });
    await alert.present();
  }
  async watchPosition() {
    try {
      this.watchId = Geolocation.watchPosition({}, (resp, err) => {
        this.ngZone.run(() => {
          if (err) {
            console.error("error", err);
            this.clearWatch();
            let curLocFound = this.pagesSelect.find(
              (o) => o.category === "curLoc"
            );
            if (curLocFound) {
              let index = this.pagesSelect.indexOf(curLocFound);
              this.pagesSelect.splice(index, 1);
            }
            if (this.platform.is("ios")) {
              //check if device is IOS cause on Android we dont need this
              this.presentAlert();
            }
            return;
          }

          this.current_location = new google.maps.LatLng(
            resp.coords.latitude,
            resp.coords.longitude
          );

          if (this.firstTimeGetLocation) {
            this?.map?.panTo(this.current_location);
            this.firstTimeGetLocation = false;
          }

          this.curLoc = {
            category: "curLoc",
            category_label: this.translate.instant("myAddresses"),
            name: this.translate.instant("myCurrentLocation"),
            address: this.current_location,
            image: "/assets/icon/custom-ion-icons/locate-outline.svg",
          };
          //add current location to pagesSelect
          let curLocFound = this.pagesSelect.find(
            (o) => o.category === "curLoc"
          );
          this.positionUpdated = true;
          this.loaderdismissed = true;
          if (curLocFound) {
            let index = this.pagesSelect.indexOf(curLocFound);
            this.pagesSelect[index] = this.curLoc;
          } else this.pagesSelect.unshift(this.curLoc);
          if (this.selected_page && this.selected_page.category == "curLoc")
            this.selected_page = this.curLoc;
          // remove old marker
          this.markerLive?.setMap(null);
          // update new marker
          this.markerLive = new google.maps.Marker({
            position: this.current_location,
            icon: this.svgMarker,
          });
          //add the new marker to the map
          this.markerLive.setMap(this.map);
        });
      });
    } catch (err) {
      console.error("error", err);
    }
  }

  clearWatch() {
    if (this.watchId != null) {
      Geolocation.clearWatch({ id: this.watchId });
    }
  }

  /**
   * Add Favorite to Favorite List
   * Update Badge for new Favorite
   * Schedule Notification for Favorite
   */
  async addFavorite() {
    this.disableButton = true;
    let start, destination;
    if (this.mapsResponse.request.origin.query) {
      start = this.mapsResponse.request.origin.query;
    } else {
      start = this.current_location.toString();
    }
    destination = this.mapsResponse.request.destination.query
      ? this.mapsResponse.request.destination.query
      : this.current_location.toString();
    this.badgeService.updateFavoriteBadge(true);
    if (
      this.mapsResponse?.routes[0]?.legs[0]?.departure_time?.value != undefined
    ) {
      await this.favoritesService.addFavorite(
        start,
        destination,
        this.mapsResponse.request.travelMode,
        this.mapsResponse.routes[0].legs[0].departure_time.value,
        new Date(),
        this.mapsResponse.routes[0].overview_polyline,
        this.currentUser,
        this.linkToAgency
      );
      this.notificationsService.scheduleNotification(
        this.mapsResponse.routes[0].legs[0].departure_time.value, 
        this.mapsResponse.request.destination.query
      );
    }
    if (
      this.mapsResponse?.routes[0]?.legs[0]?.departure_time?.value == undefined
    ) {
      await this.favoritesService.addFavorite(
        start,
        this.mapsResponse.request.destination.query,
        this.mapsResponse.request.travelMode,
        this.mapsResponse.request.transitOptions.departureTime,
        new Date(),
        this.mapsResponse.routes[0].overview_polyline,
        this.currentUser,
        "nan"
      );
      this.notificationsService.scheduleNotification(
        this.mapsResponse.request.transitOptions.departureTime, 
        this.mapsResponse.request.destination.query
      );
    }
  }

  /**
   * Create and Dismiss Loader
   */
  async createLoader(LoaderName) {
    this.loading = await this.loadingController.create({
      cssClass: "my-loading-class",
      translucent: true,
      backdropDismiss: true,
      message: this.translate.instant(LoaderName),
      duration: 200000,
    });
    await this.loading.present();
  }

  async dismissLoader() {
    await this.loading.dismiss();
    this.loaderdismissed = true;
  }

  /**
   * Setting up Listeners for all POIS including Radius of CarSharing Providers
   */
  setUpListeners() {
    let kmlResponse;
    google.maps.event.addListener(
      this.georsslayerNavigation, //first POI Icon
      "click",
      (kmlEvent) => {
        let KMLName = kmlEvent.featureData.name.replace(/-|\s/g, "").toString();
        // dynamic function name of Event Name (Name des POIs)

        // this.isDisplay = false;
        var htmlContent =
          "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
          kmlEvent.featureData.name +
          "</ion-item></b>" +
          "<ion-col><div class ='ion-text-center'><h6>" +
          (kmlEvent.featureData.description
            ? kmlEvent.featureData.description
            : "") +
          "</h6></div></ion-col>" +
          '<ion-row class="ion-align-items-center"> <ion-col  class="ion-text-center"><ion-button id="' +
          this.id +
          '"  color="secondary" expand="block"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col></ion-row>';

        if (this.pages[kmlEvent.featureData.name]) {
          htmlContent =
            "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
            kmlEvent.featureData.name +
            "</ion-item></b>" +
            "<ion-col><div class ='ion-text-center'><h6>" +
            (kmlEvent.featureData.description
              ? kmlEvent.featureData.description
              : "") +
            "</h6></div></ion-col>" +
            '<ion-row class="ion-align-items-center"> <ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.id +
            '"  color="secondary"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col>' +
            '<ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.infoID +
            '"  color="primary"><ion-icon name="information-circle-outline"></ion-icon></ion-button></ion-col> </ion-row>';
        }

        this.infowindow.setOptions({
          content: htmlContent,
          pixelOffset: kmlEvent.pixelOffset,
          position: kmlEvent.latLng,
        });

        if (
          this.available_POIS.includes(
            kmlEvent.featureData.name.replace(/-|\s/g, "").toString()
          )
        ) {
          // only call function when found in Array of flexible mobility POIs
          this.showRadius(KMLName);
        }
        this.infowindow.open(this.map);
        this.poiPosition = kmlEvent.latLng;
        this.directionForm.patchValue({ destination: this.poiPosition });
        kmlResponse = kmlEvent;
      }
    );
    google.maps.event.addListener(
      this.georsslayerFreizeit, // second POI icon
      "click",
      (kmlEvent) => {
        // this.isDisplay = false;
        var htmlContent =
          "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
          kmlEvent.featureData.name +
          "</ion-item></b>" +
          "<ion-col><div class ='ion-text-center'><h6>" +
          (kmlEvent.featureData.description
            ? kmlEvent.featureData.description
            : "") +
          "</h6></div></ion-col>" +
          '<ion-row class="ion-align-items-center"> <ion-col  class="ion-text-center"><ion-button id="' +
          this.id +
          '"  color="secondary" expand="block"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col></ion-row>';

        if (this.pages[kmlEvent.featureData.name]) {
          htmlContent =
            "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
            kmlEvent.featureData.name +
            "</ion-item></b>" +
            "<ion-col><div class ='ion-text-center'><h6>" +
            (kmlEvent.featureData.description
              ? kmlEvent.featureData.description
              : "") +
            "</h6></div></ion-col>" +
            '<ion-row class="ion-align-items-center"> <ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.id +
            '"  color="secondary"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col>' +
            '<ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.infoID +
            '"  color="primary"><ion-icon name="information-circle-outline"></ion-icon></ion-button></ion-col> </ion-row>';
        }

        this.infowindow.setOptions({
          content: htmlContent,
          pixelOffset: kmlEvent.pixelOffset,
          position: kmlEvent.latLng,
        });

        this.infowindow.open(this.map);
        this.poiPosition = kmlEvent.latLng;
        this.directionForm.patchValue({ destination: this.poiPosition });
        kmlResponse = kmlEvent;
      }
    );
    google.maps.event.addListener(this.georsslayerElse, "click", (kmlEvent) => {
      //third POI icon
      // this.isDisplay = false;
      var htmlContent =
        "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
        kmlEvent.featureData.name +
        "</ion-item></b>" +
        "<ion-col><div class ='ion-text-center'><h6>" +
        (kmlEvent.featureData.description
          ? kmlEvent.featureData.description
          : "") +
        "</h6></div></ion-col>" +
        '<ion-row class="ion-align-items-center"> <ion-col class="ion-text-center"><ion-button id="' +
        this.id +
        '"  color="secondary" expand="block"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col></ion-row>';

      if (this.pages[kmlEvent.featureData.name]) {
        htmlContent =
          "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
          kmlEvent.featureData.name +
          "</ion-item></b>" +
          "<ion-col><div class ='ion-text-center'><h6>" +
          (kmlEvent.featureData.description
            ? kmlEvent.featureData.description
            : "") +
          "</h6></div></ion-col>" +
          '<ion-row class="ion-align-items-center"> <ion-col size="6" class="ion-text-center"><ion-button id="' +
          this.id +
          '"  color="secondary"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col>' +
          '<ion-col size="6" class="ion-text-center"><ion-button id="' +
          this.infoID +
          '"  color="primary"><ion-icon name="information-circle-outline"></ion-icon></ion-button></ion-col> </ion-row>';
      }

      this.infowindow.setOptions({
        content: htmlContent,
        pixelOffset: kmlEvent.pixelOffset,
        position: kmlEvent.latLng,
      });

      this.infowindow.open(this.map);
      this.poiPosition = kmlEvent.latLng;
      this.directionForm.patchValue({ destination: this.poiPosition });
      kmlResponse = kmlEvent;
    });
    google.maps.event.addListener(
      this.georsslayerInformation, // fourth POI icon
      "click",
      (kmlEvent) => {
        // this.isDisplay = false;
        var htmlContent =
          "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
          kmlEvent.featureData.name +
          "</ion-item></b>" +
          "<ion-col><div class ='ion-text-center'><h6>" +
          (kmlEvent.featureData.description
            ? kmlEvent.featureData.description
            : "") +
          "</h6></div></ion-col>" +
          '<ion-row class="ion-align-items-center"> <ion-col  class="ion-text-center"><ion-button id="' +
          this.id +
          '"  color="secondary" expand="block"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col></ion-row>';

        if (this.pages[kmlEvent.featureData.name]) {
          htmlContent =
            "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
            kmlEvent.featureData.name +
            "</ion-item></b>" +
            "<ion-col><div class ='ion-text-center'><h6>" +
            (kmlEvent.featureData.description
              ? kmlEvent.featureData.description
              : "") +
            "</h6></div></ion-col>" +
            '<ion-row class="ion-align-items-center"> <ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.id +
            '"  color="secondary"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col>' +
            '<ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.infoID +
            '"  color="primary"><ion-icon name="information-circle-outline"></ion-icon></ion-button></ion-col> </ion-row>';
        }

        this.infowindow.setOptions({
          content: htmlContent,
          pixelOffset: kmlEvent.pixelOffset,
          position: kmlEvent.latLng,
        });

        this.infowindow.open(this.map);
        this.poiPosition = kmlEvent.latLng;
        this.directionForm.patchValue({ destination: this.poiPosition });
        kmlResponse = kmlEvent;
      }
    );
    google.maps.event.addListener(
      this.georsslayerMitfahrbaenke, // fourth POI icon
      "click",
      (kmlEvent) => {
        // this.isDisplay = false;
        var htmlContent =
          "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
          kmlEvent.featureData.name +
          "</ion-item></b>" +
          "<ion-col><div class ='ion-text-center'><h6>" +
          (kmlEvent.featureData.description
            ? kmlEvent.featureData.description
            : "") +
          "</h6></div></ion-col>" +
          '<ion-row class="ion-align-items-center"> <ion-col  class="ion-text-center"><ion-button id="' +
          this.id +
          '"  color="secondary" expand="block"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col></ion-row>';

        if (this.pages[kmlEvent.featureData.name]) {
          htmlContent =
            "<ion-item color='secondary'; text='center'; class='my-text'> <b>" +
            kmlEvent.featureData.name +
            "</ion-item></b>" +
            "<ion-col><div class ='ion-text-center'><h6>" +
            (kmlEvent.featureData.description
              ? kmlEvent.featureData.description
              : "") +
            "</h6></div></ion-col>" +
            '<ion-row class="ion-align-items-center"> <ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.id +
            '"  color="secondary"><ion-icon name="navigate-outline"></ion-icon></ion-button></ion-col>' +
            '<ion-col size="6" class="ion-text-center"><ion-button id="' +
            this.infoID +
            '"  color="primary"><ion-icon name="information-circle-outline"></ion-icon></ion-button></ion-col> </ion-row>';
        }

        this.infowindow.setOptions({
          content: htmlContent,
          pixelOffset: kmlEvent.pixelOffset,
          position: kmlEvent.latLng,
        });

        this.infowindow.open(this.map);
        this.poiPosition = kmlEvent.latLng;
        this.directionForm.patchValue({ destination: this.poiPosition });
        kmlResponse = kmlEvent;
      }
    );
    google.maps.event.addListener(this.infowindow, "closeclick", () => {
      this.removeRadius();
      //removes the marker
      // then, remove the infowindows name from the array
    });
    google.maps.event.addListener(this.infowindow, "domready", () => {
      document.getElementById(this.id).addEventListener("click", () => {
        this.destination = this.poiPosition;
        this.calculateServiceRoute();
      });
    });

    google.maps.event.addListener(this.infowindow, "domready", () => {
      document.getElementById(this.infoID)?.addEventListener("click", () => {
        // GOTO specific POI
        this.router.navigate([
          "app/infohub/info/" + kmlResponse.featureData.name,
        ]);
      });
    });
  }

  /**
   * Go To Current Position
   */
  public async goToCurrentPosition() {
    await this.environmentService.createLoader(() => this.getMyLocation());
    this?.map?.panTo(this.current_location);
    const KEY = ""; //INSERT KEY HERE!
    const LAT = this.current_location?.lat();
    const LNG = this.current_location?.lng();

    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`;
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        this.geocoded_current_location =
          data.results[0].formatted_address.toString();
        this.dataIsFetched = true;
      });
    await waitUntil(() => this.dataIsFetched == true);
    this.dataIsFetched = false;
    this.directionForm.patchValue({ start: this.geocoded_current_location });
  }

  async calculateServiceRoute() {
    if (!this.mode) this.mode = "TRANSIT";
    if (!this.isDeparture) this.isDeparture = true;
    if (!this.myMapsDate) this.myMapsDate = new Date();
    if (!this.start) {
      await waitUntil(() => this.current_location != null);
      this.start = this.current_location;
    }
    if (!this.destination) {
      return;
    }
    // if (fromMap) this.destination = destination;
    await this.routeCalculationService.setcalculateAndDisplayRoute(
      this.mode,
      this.isDeparture,
      this.myMapsDate,
      this.start,
      this.destination
    );
    var response = await this.routeCalculationService.getResponse();
    this.mapsResponse = response;
    this.linkToAgency = await this.routeCalculationService.getlinkToAgency();
    if (this.routeCalculationService.getIsTransit()) {
      this.isTransit = true;
    }
    this.directionsDisplay.setMap(this.map);
    /*Remove when outsourced to single page*/
    // this.isDisplay = false;
    this.disableButton = false;
    /***************************************/
    this.environmentService.setArrivalTime(
      response.routes[0].legs[0].arrival_time?.value
    );
    this.footerState = IonPullUpFooterState.Expanded;

    this.showDirectionPanel = true; //show panel
    this.selected_dest = this.environmentService.getSelectedDest();

    if (this?.selected_dest?.uid === undefined) {
      this.disableChatButton = true;
    } else this.disableChatButton = false;
    this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);
    this.infowindow.close();
    this.georsslayerNavigation.setMap(null);
    this.georsslayerElse.setMap(null);
    this.georsslayerFreizeit.setMap(null);
    this.georsslayerMitfahrbaenke.setMap(null);
    this.directionsDisplay.setDirections(response);
  }

  applyHomeAdressStart() {
    if (this.adress != null) {
      this.directionForm.patchValue({ start: this.adress });
    } else {
      this.alertService.presentAlertGoTo(
        this.translate.instant("HeimatadresseFehlt"),
        this.translate.instant("ZuSettings"),
        this.translate.instant("CANCEL"),
        this.translate.instant("Settings"),
        "app/categories/userSettings"
      );
    }
  }
  async applyHomeAdressDest() {
    if (this.adress != null) {
      this.directionForm.patchValue({
        destination: { address: this.adress },
      });
      let found = await this.pagesSelect.find(
        (e) => e.category && e.category === "home"
      );
      this.selected_page = found;
      // setTimeout(() => {
      //   this.selected_page = found;
      // }, 20);
    } else {
      this.alertService.presentAlertGoTo(
        this.translate.instant("HeimatadresseFehlt"),
        this.translate.instant("ZuSettings"),
        this.translate.instant("CANCEL"),
        this.translate.instant("Settings"),
        "app/categories/userSettings"
      );
    }
  }

  blankSelectedPage() {
    this.selected_page = null;
  }

  /***********************************************
   *
   ************************************************/

  ngOnDestroy(): void {
    this.clearWatch();
    this.subscriptions?.unsubscribe();
  }
  async calculateFromInfoHub() {
    // await waitUntil(() => this.current_location != null);
    this.start = window.history.state.route?.from;
    this.destination = window.history.state.route?.to;
    let departureTime = window.history.state.route?.departureTime;
    if (departureTime) {
      this.myMapsDate = departureTime;
      this.isDeparture = true;
    } else {
      this.myMapsDate = new Date();
      this.isDeparture = true;
    }
    if (!this.start) {
      const KEY = "";//INSERT KEY HERE!
      const LAT = this.current_location.lat();
      const LNG = this.current_location.lng();
      let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${LAT},${LNG}&key=${KEY}`;
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          this.geocoded_current_location =
            data.results[0].formatted_address.toString();
          this.start = this.geocoded_current_location;
          this.curLoc = {
            category: "curLoc",
            category_label: this.translate.instant("myAddresses"),
            name: this.translate.instant("myCurrentLocation"),
            address: this.geocoded_current_location,
            image: "/assets/icon/custom-ion-icons/locate-outline.svg",
          };
          this.environmentService.setSelectedStart(this.curLoc);
          this.dataIsFetched = true;
        });

      await waitUntil(() => this.dataIsFetched == true);
      this.dataIsFetched = false;
    }

    await waitUntil(() => this.pagesSelectReady == true);
    let found = await this.pagesSelect.find(
      (e) => e.address === this.destination
    );
    this.selected_dest = found;
    this.environmentService.setSelectedDest(this.selected_dest);
    // setTimeout(() => {
    //   this.selected_dest = found;
    //   this.environmentService.setSelectedDest(this.selected_dest);
    // }, 20);
    this.directionForm.patchValue({
      start: this.start,
      destination: found,
    });
    this.calculateServiceRoute();
  }

  async ionViewWillEnter() {
    // get current home address
    this.storage.get("adress").then((value: any) => {
      this.adress = value;
      //add home to pagesSelect
      if (this.adress) {
        let isHomeExisted = this.pagesSelect.find((o) => o.category === "home");
        let home = {
          category: "home",
          category_label: this.translate.instant("myAddresses"),
          name: this.translate.instant("myHome"),
          address: this.adress,
          image: "/assets/icon/custom-ion-icons/md-gps-house.svg",
        };
        if (isHomeExisted) {
          let indexOfHome = this.pagesSelect.indexOf(isHomeExisted);
          this.pagesSelect[indexOfHome] = home;
        } else this.pagesSelect.unshift(home);
      }
      // else this.pagesSelect.shift();
    });
    // initialize Logos
    if (window.history.state.route) {
      await this.getMyLocation();
      //boolean request comes from favorite or infohub

      this.calculateFromInfoHub();
    }
    if (window.history.state.card) {
      // this.isDisplay = false;
      this.toggleKML("info");
    }
  }
  async ngOnInit() {
    this.userRole = await this.userInfoService.getUserRole();
    this.currentUser = await this.userInfoService.getCurrentUser(); // get current user ID
    // initialize listeners
    this.setUpListeners();
    this.integration = this.logoService.getIntegrationLogo();
    this.behoerde = this.logoService.getBehoerdeLogo();
    this.mobility = this.logoService.getMobilityLogo();
    this.freetime = this.logoService.getFreetimeLogo();
    this.integrationColor = this.logoService.getIntegrationColor();
    this.behoerdeColor = this.logoService.getBehoerdeColor();
    this.mobilityColor = this.logoService.getMobilityColor();
    this.freetimeColor = this.logoService.getFreetimeColor();
    // initialize DB
    this.db
      .collection("pages")
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let data = this.data;
          data = doc.data();
          this.pages[doc.id] = {
            id: doc.id,
            name: data["name_" + this.translate.currentLang],
            icon: data.icon,
            address: data.address,
          };
        });
      });
    await this.findPages();

    this.routeCalculationService.getRequest().subscribe((request) => {
      if (request) {
        this.mode = request.mode;
        this.isDeparture = request.departure;
        this.myMapsDate = request.mapsDate;
        this.start = request.start;
        this.destination = request.destination;

        this.calculateServiceRoute();
      }
    });

    /**
     * test: initiative get route data from user
     */
    if (this.userRole == "INITIATIVE") {
      this.db
        .collectionGroup("myRoutes", (ref) =>
          ref.where("initiativeId", "==", this.currentUser)
        )
        .valueChanges()
        .subscribe((docs) => {
          console.log("routes to this inititive", docs);
        });
    } else {
      /**
       * user get their route data
       */
      this.db
        .collection("normalUser")
        .doc(this.currentUser)
        .collection("myRoutes")
        .valueChanges()
        .subscribe((docs) => {
          console.log("my routes", docs);
        });
    }
  }

  async findPages() {
    await this.db
      .collection("pages", (ref) => ref.where("address", "!=", null))
      .get()
      .subscribe((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let data = this.data;
          data = doc.data();

          this.pagesSelect.push({
            id: doc.id,
            name: data["name_" + this.translate.currentLang],
            icon: data.icon,
            address: data.address,
            image: data.imageURL,
            uid: data.uid,
            category: "poi",
            category_label: this.translate.instant("Anlaufstelle"),
          });
        });

        this.pagesSelectReady = true;
      });
  }

  ngAfterViewInit() {
    // GoogleMapComponent should be available
    this._GoogleMap.$mapReady.subscribe((map) => {
      this.map = map;
      console.log("ngAfterViewInit - Google map ready");
    });
  }
}
