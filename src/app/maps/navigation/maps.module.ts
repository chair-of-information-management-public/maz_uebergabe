import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MapsPage } from "./maps.page";
import { ComponentsModule } from "../../components/components.module";
import { TranslateModule } from "@ngx-translate/core";
import { IonicPullupModule } from "ionic-pullup";
import { NgxAutocomPlaceModule } from "ngx-autocom-place";
import { IonicSelectableModule } from "ionic-selectable";
import { VgCoreModule } from "@videogular/ngx-videogular/core";
import { VgControlsModule } from "@videogular/ngx-videogular/controls";
import { VgOverlayPlayModule } from "@videogular/ngx-videogular/overlay-play";
import { VgBufferingModule } from "@videogular/ngx-videogular/buffering";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ComponentsModule,
    TranslateModule,
    IonicSelectableModule,
    FormsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    NgxAutocomPlaceModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: "", component: MapsPage }]),
    IonicPullupModule,
  ],
  declarations: [MapsPage],
})
export class MapsPageModule {}
