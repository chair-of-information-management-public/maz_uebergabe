import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DirectionFormPage } from './direction-form.page';

describe('DirectionFormPage', () => {
  let component: DirectionFormPage;
  let fixture: ComponentFixture<DirectionFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectionFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DirectionFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
