import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DirectionFormPage } from './direction-form.page';

const routes: Routes = [
  {
    path: '',
    component: DirectionFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DirectionFormPageRoutingModule {}
