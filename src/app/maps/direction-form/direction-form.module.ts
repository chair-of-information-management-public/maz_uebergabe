import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { DirectionFormPageRoutingModule } from "./direction-form-routing.module";
import { IonicSelectableModule } from "ionic-selectable";
import { DirectionFormPage } from "./direction-form.page";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DirectionFormPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    IonicSelectableModule,
  ],
  declarations: [DirectionFormPage],
})
export class DirectionFormPageModule {}
