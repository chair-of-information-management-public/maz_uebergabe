import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ModalController } from "@ionic/angular";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { IonicSelectableComponent } from "ionic-selectable";
import { MapsService } from "../../services/maps.service";
import { RouteCalculationService } from "../../services/route-calculation.service";
import waitUntil from "async-wait-until";
import { Environment } from "../../services/environment.service";

@Component({
  selector: "app-direction-form",
  templateUrl: "./direction-form.page.html",
  styleUrls: ["./direction-form.page.scss"],
})
export class DirectionFormPage implements OnInit {
  @Input() pagesSelect: Array<any>;
  otherResults: any[];
  directionForm: FormGroup;
  selected_start: any;
  selected_dest: any;
  /** */
  isDeparture = true;
  myDate = new Date().toISOString();
  myMapsDate = new Date();
  mode = "TRANSIT";

  @ViewChild("inputStart") inputStart: IonicSelectableComponent;
  @ViewChild("inputDestination") inputDestination: IonicSelectableComponent;
  constructor(
    private modalController: ModalController,
    private fb: FormBuilder,
    public translate: TranslateService,
    public mapsService: MapsService,
    private routeCalculationService: RouteCalculationService,
    private environmentService: Environment
  ) {
    this.createDirectionForm();
    this.selected_start = this.environmentService.getSelectedStart();
    this.selected_dest = this.environmentService.getSelectedDest();
  }

  ngOnInit() {}

  createDirectionForm() {
    //Form for user to fill
    this.directionForm = this.fb.group({
      start: ["", Validators.required],
      destination: ["", Validators.required],
    });
    this.selected_start = this.environmentService.getSelectedPage();
  }
  timeModeChanged() {
    this.isDeparture = !this.isDeparture;
  }
  dateChanged() {
    [this.myDate, this.myMapsDate] = this.mapsService.stringToDate(
      this.myDate,
      this.myMapsDate
    );
  }
  segmentChanged($event) {
    this.mode = $event.detail.value;
  }
  onSearchFail(event: { component: IonicSelectableComponent; text: string }) {
    this.otherResults = [];
    let searchText = event.text;
    var sessionToken = new google.maps.places.AutocompleteSessionToken();
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getPlacePredictions(
      {
        input: searchText,
        sessionToken: sessionToken,
      },
      (res) => {
        if (res.length === 0) {
          return;
        }
        res.forEach((item) => {
          this.otherResults.push({ address: item.description });
        });
      }
    );
  }
  radioGroupChangeStart($event) {
    this.selected_start = {
      name: $event.detail.value,
      address: $event.detail.value,
    };
    this.inputStart.close();
  }
  radioGroupChangeDest($event) {
    this.selected_dest = {
      name: $event.detail.value,
      address: $event.detail.value,
    };
    this.inputDestination.close();
  }
  dismissModal() {
    this.modalController.dismiss({
      dismissed: true,
    });
    this.environmentService.setSelectedStart(this.selected_start);
    this.environmentService.setSelectedDest(this.selected_dest);
  }
  sendRouteRequestToMap() {
    this.routeCalculationService.setRequest(
      this.mode,
      this.isDeparture,
      this.myMapsDate,
      this.directionForm.value.start.address,
      this.directionForm.value.destination.address
    );
    this.dismissModal();
  }
}
