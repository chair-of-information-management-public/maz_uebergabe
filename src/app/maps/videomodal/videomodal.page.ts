import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ViewChild } from '@angular/core';
import { VgApiService, VgMediaDirective } from '@videogular/ngx-videogular/core';

@Component({
  selector: 'app-videomodal',
  templateUrl: './videomodal.page.html',
  styleUrls: ['./videomodal.page.scss'],
})
export class VideomodalPage implements OnInit {
  
  @ViewChild(VgMediaDirective, { static: true }) media: VgMediaDirective;
  api:VgApiService;
  urlVideo:string=""
  items = [
    {
      "title":"External file",
      "url":"http://static.videogular.com/assets/videos/videogular.mp4",
      "imagePreview":"assets/earth.png"
    },
    {
      "title":"Local video file",
      "url":"assets/videos/MAZ Ticketkauf_free.mp4",
      "imagePreview":"assets/earth.png"
    },
  ]

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }
  dismissModal(){
    this.modalController.dismiss({
     'dismissed': true
    })
  }
  playVideo(item){
   
    // Play video
    this.urlVideo=item.url
    if (this.media){
       this.media.vgMedia.src=this.urlVideo
       this.media.subscriptions.canPlay.subscribe((value)=>{
        //this.api.fsAPI.toggleFullscreen()
        this.media.play()
       })
    }
  }
  onPlayerReady(api:VgApiService){
    this.api = api
    this.urlVideo = this.items[1].url
    this.api.fsAPI.toggleFullscreen()
  }

}
