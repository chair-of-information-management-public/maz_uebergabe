import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VideomodalPage } from './videomodal.page';

describe('VideomodalPage', () => {
  let component: VideomodalPage;
  let fixture: ComponentFixture<VideomodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideomodalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VideomodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
