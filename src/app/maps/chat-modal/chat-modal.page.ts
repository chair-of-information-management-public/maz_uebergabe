import { Component, OnInit } from "@angular/core";
import { ModalController, ToastController } from "@ionic/angular";
import { ChatService } from "../../services/chat.service";
import { BadgeService } from "../../services/badge.service";
import { Environment } from "../../services/environment.service";
import { TranslateService } from "@ngx-translate/core";
import { UserInfoService } from "../../services/userInfo.service";

@Component({
  selector: "app-chat-modal",
  templateUrl: "./chat-modal.page.html",
  styleUrls: ["./chat-modal.page.scss"],
})
export class ChatModalPage implements OnInit {
  selectedPage;
  message;
  arrivalDate: string;
  arrivalTime: string;
  constructor(
    public modalController: ModalController,
    private environmentService: Environment,
    private chatService: ChatService,
    private badgeService: BadgeService,
    private toastController: ToastController,
    public translate: TranslateService,
    private userInfo: UserInfoService
  ) {
    this.selectedPage = this.environmentService.getSelectedDest();
    this.createAutoMessage().then((mes) => {
      this.message = mes;
    });
  }

  async ngOnInit() {
    // get current user ID
    await this.userInfo.getCurrentUser();
    // find the chat with selected user (page)
    await this.chatService.setChatID(this.selectedPage.uid);
  }
  async createAutoMessage() {
    this.arrivalDate = await this.environmentService
      .getArrivalTime()
      .toLocaleDateString();
    this.arrivalTime = await this.environmentService
      .getArrivalTime()
      .toLocaleTimeString();
    this.arrivalTime = this.arrivalTime.substr(0, 5);
    return `Hallo. Ich komme am ${this.arrivalDate} um circa ${this.arrivalTime} bei Ihnen an. Bis bald!`;
  }
  dismissModal(routeConfirmed: boolean) {
    this.modalController.dismiss({
      dismissed: true,
      routeConfirmed: routeConfirmed,
    });
  }
  addChatMessage(msg, page_uid) {
    this.chatService.addChatMessage(msg, page_uid).then(() => {
      this.presentToast();
    });
    this.badgeService.updateMessageBadge(true);
    this.dismissModal(true);
  }

  async presentToast() {
    // const Einstellungen = this.translate.instant('Einstellungen');

    const toast = await this.toastController.create({
      message: this.translate.instant("MessageSend"),
      duration: 3000,
    });
    toast.present();
  }
}
