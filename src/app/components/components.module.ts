import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";

import { ShellModule } from "../shell/shell.module";

import { CheckboxWrapperComponent } from "./checkbox-wrapper/checkbox-wrapper.component";
import { ShowHidePasswordComponent } from "./show-hide-password/show-hide-password.component";
import { RatingInputComponent } from "./rating-input/rating-input.component";
import { GoogleMapComponent } from "./google-map/google-map.component";
import { ExpandableComponent } from "./expandable/expandable.component";
import { PostComponent } from "./post/post.component";
import { CommentComponent } from "./comment/comment.component";

@NgModule({
  imports: [CommonModule, FormsModule, ShellModule, IonicModule],
  declarations: [
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    RatingInputComponent,
    GoogleMapComponent,
    ExpandableComponent,
    PostComponent,
    CommentComponent,
  ],
  exports: [
    ShellModule,
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    RatingInputComponent,
    GoogleMapComponent,
    ExpandableComponent,
    PostComponent,
    CommentComponent,
  ],
})
export class ComponentsModule {}
