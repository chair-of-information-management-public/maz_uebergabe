import { Component, OnInit, Input } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import { CommentEditPage } from "../../bulletin-board/comments/comment-edit/comment-edit.page";
import { BulletinBoardService } from "../../services/bulletin-board.service";
import { UserInfoService } from "../../services/userInfo.service";

@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.scss"],
})
export class CommentComponent implements OnInit {
  uid;
  @Input() comment: any;
  constructor(
    private userInfo: UserInfoService,
    private BulletinService: BulletinBoardService,
    private alertController: AlertController,
    private modalController: ModalController
  ) {}

  async ngOnInit() {
    this.parseComment();
    this.uid = await this.userInfo.getCurrentUser();
  }
  // Transform comment to specific form
  parseComment() {
    this.userInfo.getUserInfoFromId(this.comment.authorId).then((info) => {
      this.comment.author = info.username;
      this.comment.authorAvatar = info.photoURL;
    });
    this.comment.content = this.comment.content.replace(
      /\#[a-zA-Z]+/g,
      '<span class="highlight">$&</span>'
    );
  }

  // delete an existing comment
  deleteComment() {
    this.BulletinService.deleteComment(
      this.uid,
      window.history.state.postId,
      this.comment.commentId
    );
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: "Kommentar löschen?",
      message: "Möchten Sie diesen Kommentar wirklich löschen?", 
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: "Okay",
          handler: () => {
            this.deleteComment();
          },
        },
      ],
    });

    await alert.present();
  }
  async presentCommentEditModal() {
    const modal = await this.modalController.create({
      component: CommentEditPage,
      componentProps: {
        authorId: this.comment.authorId,
        postId: window.history.state.postId,
        commentId: this.comment.commentId,
        content: this.comment.content,
      },
    });
    return await modal.present();
  }
}
