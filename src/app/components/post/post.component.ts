import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Route, Router } from "@angular/router";
import { UserInfoService } from "../../services/userInfo.service";
import { BulletinBoardService } from "../../services/bulletin-board.service";
import { unescapeIdentifier } from "@angular/compiler";
import { AlertController, ModalController } from "@ionic/angular";
import { PostEditPage } from "../../bulletin-board/post-edit/post-edit.page";
import { FavoritesService } from "../../services/favorites.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"],
})
export class PostComponent implements OnInit {
  uid;
  @Input() post: any;
  date;
  today;
  constructor(
    private router: Router,
    private userInfo: UserInfoService,
    private BulletinBoardService: BulletinBoardService,
    private modalController: ModalController,
    private alertController: AlertController,
    private favoriteService: FavoritesService,
    private translate: TranslateService
  ) {}

  async ngOnInit() {
    this.uid = await this.userInfo.getCurrentUser();
    this.parsePost();
    this.date = new Date(this.post.departureTime);
    this.today = new Date();
    this.today.setDate(this.today.getDate() + 1);
  }

  parsePost() {
    this.userInfo.getUserInfoFromId(this.post.authorId).then((info) => {
      this.post.author = info.username;
      this.post.authorAvatar = info.photoURL;
    });
    this.post.content = this.post.content.replace(
      /\#[a-zA-Z]+/g,
      '<span class="highlight">$&</span>'
    );
    this.favoriteService.isLiked(this.post.postId, this.uid).then((isLiked) => {
      this.post.liked = isLiked;
    });
  }
  async presentPostEditModal() {
    const modal = await this.modalController.create({
      component: PostEditPage,
      componentProps: {
        isEdit: true,
        postId: this.post.postId,
      },
    });
    return await modal.present();
  }
  renderComments(postId) {
    this.router.navigate(["app/bulletin-board/comments"], {
      state: { postId: postId },
    });
  }

  async participateInRide() {
    try {
      await this.BulletinBoardService.updateNumberOfSeatsTaken(
        this.post.postId,
        this.uid,
        1
      );
    } catch (err) {
      window.alert(err);
    }

    // this.post.seatsTaken++;
    // this.post.userOfSeatsTaken.push(this.uid);
    if (!this.post.liked) this.likePost();
  }
  async leaveFromRide() {
    await this.BulletinBoardService.updateNumberOfSeatsTaken(
      this.post.postId,
      this.uid,
      -1
    );

    // this.post.seatsTaken--;
    // this.post.userOfSeatsTaken.splice(
    //   this.post.userOfSeatsTaken.indexOf(this.uid),
    //   1
    // );
  }
  deletePost() {
    this.BulletinBoardService.deletePost(this.uid, this.post.postId);
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: "Beitrag löschen?",
      message: "Möchten Sie den Beitrag wirklich löschen?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: "Okay",
          handler: () => {
            this.deletePost();
          },
        },
      ],
    });

    await alert.present();
  }

  async likePost() {
    this.post.liked = !this.post.liked;
    if (this.post.liked) {
      await this.favoriteService.addLike(this.uid, this.post.postId);
    } else await this.favoriteService.unLike(this.uid, this.post.postId);
  }
}
