import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { FavoritesService } from './favorites.service';

@Injectable()
export class FavoritesResolver implements Resolve<any> {

  constructor(private favoritesService: FavoritesService) { }

  resolve() {
    // Base Observable (where we get data from)
    const dataObservable: Observable<any> = this.favoritesService.getData();

    return { source: dataObservable };
  }
}
