import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FavoritesPage } from "./favorites.page";
import { ComponentsModule } from "../components/components.module";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ComponentsModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: "", component: FavoritesPage }]),
  ],
  declarations: [FavoritesPage],
})
export class FavoritesPageModule {}
