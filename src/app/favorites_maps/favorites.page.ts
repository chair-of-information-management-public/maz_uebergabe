import { Component, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Observable, Subscription } from "rxjs";
import { LanguageService } from "../language/language.service";
import { first } from "rxjs/operators";
import { LogoService } from "../services/logo.service";
import { UserInfoService } from "../services/userInfo.service";
import { FavoritesService } from "../services/favorites.service";
import { User } from "../services/userInfo.service";
import { LoadingController, ToastController } from "@ionic/angular";
import { GoogleMapComponent } from "../components/google-map/google-map.component";
import { DatePipe } from "@angular/common";
import { Environment } from "../services/environment.service";
import { LocalNotificationsService } from "../services/local-notifications.service";
import { link } from "fs";
import { waitUntil } from "async-wait-until";

@Component({
  selector: "app-favorites",
  templateUrl: "./favorites.page.html",
  styleUrls: [
    "./styles/maps.page.scss",
    "./styles/favorites.shell.scss",
    "./styles/favorites.ios.scss",
  ],
})
export class FavoritesPage implements AfterViewInit {
  @ViewChild(GoogleMapComponent, { static: false })
  _GoogleMap: GoogleMapComponent;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  map: google.maps.Map;
  favoriteRoutes = null;
  favoriteRoutes2 = [];
  tmp: any;
  data: any;
  searchbar;
  loadingElement: any;
  currentUser: any;
  contentLoaded = false;
  sliderOpts = {
    zoom: {
      maxRatio: 2,
    },
  };
  hideMe = true;
  translations;
  subscriptions: Subscription;
  mapOptions: google.maps.MapOptions = {
    zoom: 15,
    center: { lat: -34.9199842, lng: -56.149849 },
    mapTypeControl: false,
    fullscreenControl: false,
    zoomControl: false,
    // uncomment the following line if you want to remove the default Map controls
    // disableDefaultUI: true
  };
  @ViewChild("directionsPanel") directionsPanel: ElementRef;
  constructor(
    private db: AngularFirestore,
    public languageService: LanguageService,
    private localNotificationService: LocalNotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    public datepipe: DatePipe,
    public toastController: ToastController,
    private logoService: LogoService,
    private translate: TranslateService,
    private userInfoService: UserInfoService,
    private favoritesService: FavoritesService,
    private loadingController: LoadingController,
    public environmentService: Environment
  ) {}

  async ngOnInit() {
    this.contentLoaded = false;
    this.currentUser = await this.userInfoService.getCurrentUser(); // get current user ID
  }
  /**
   * Delete the saved route
   * @param route
   */
  async deleteRoute(route) {
    this.favoritesService.deleteFavorite(route.id, this.currentUser);
    this.favoriteRoutes = this.favoriteRoutes.filter((x) => {
      return x.id != route.id;
    });
    const routeDeleted = this.translate.instant("RouteDeleted");
    var routeID = Number(route.date);
    this.localNotificationService.cancelNotification(route.dateOrig);
    const toast = await this.toastController.create({
      message: routeDeleted,
      duration: 3000,
    });
    toast.present();
  }

  ngAfterViewInit() {
    // GoogleMapComponent should be available
    this._GoogleMap?.$mapReady.subscribe((map) => {
      this.map = map;
      console.log("ngAfterViewInit - Google map ready");
    });
    this.createLoader();
  }
  async createLoader() {
    this.loadingElement = await this.loadingController.create({
      message: "Trying to get your current location...",
    });
  }
  async presentLoader() {
    await this.loadingElement.present();
  }

  async dismissLoader() {
    if (this.loadingElement) {
      await this.loadingElement.dismiss();
    }
  }

  ionViewDidLoad() {}
  async ionViewWillEnter() {
    this.favoriteRoutes = await this.favoritesService.initializeItems();
    this.environmentService.createLoader(() => {
      return new Promise((res, rej) => {
        if (this.favoriteRoutes != null) res();
      });
    });
    await waitUntil(() => this.favoriteRoutes != null);

    this.contentLoaded = false;
    this.currentUser = await this.userInfoService.getCurrentUser(); // get current user ID
    setTimeout(() => (this.contentLoaded = true), 200);
  }

  /**
   * open link to ticket agency
   * @param linkToAgency
   */
  openAgencyLink(linkToAgency) {
    window.open(linkToAgency);
  }
}
export interface Route {
  id: string;
  datePiped: Date;
  dateOrig: Date;
  from: string;
  to: string;
  imageSRC: string;
  staticMapImage: string;
  linkToAgency: any;
}
