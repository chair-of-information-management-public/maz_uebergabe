import { NgModule } from "@angular/core";
import { AngularFireAuthGuard } from "@angular/fire/auth-guard";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "",
    component: TabsPage,
    children: [
      // /app/ redirect
      {
        path: "notifications",
        loadChildren: () =>
          import("../notifications/notifications.module").then(
            (m) => m.NotificationsPageModule
          ),
      },
      {
        path: "chat",
        loadChildren: () =>
          import("../chat/contact-list/contact-list.module").then(
            (m) => m.ContactListPageModule
          ),
      },
      {
        path: "favorites",
        loadChildren: () =>
          import("../favorites_maps/favorites.module").then(
            (m) => m.FavoritesPageModule
          ),
      },
      {
        path: "maps",
        children: [
          { path: "", redirectTo: "maps" },
          {
            path: "maps",
            loadChildren: () =>
              import("../maps/navigation/maps.module").then(
                (m) => m.MapsPageModule
              ),
          },
        ],
      },
      {
        path: "favorites",
        canActivate: [AngularFireAuthGuard],
        loadChildren: () =>
          import("../favorites_maps/favorites.module").then(
            (m) => m.FavoritesPageModule
          ),
      },
      {
        path: "categories",
        canActivate: [AngularFireAuthGuard],
        loadChildren: () =>
          import("../infohub/categories/categories.module").then(
            (m) => m.CategoriesPageModule
          ),
      },
      {
        path: "infohub/infopdfs",
        canActivate: [AngularFireAuthGuard],
        loadChildren: () =>
          import("../infohub/infopdfs/infopdfs.module").then(
            (m) => m.InfopdfsPageModule
          ),
      },
      {
        path: "infohub/tickets",
        canActivate: [AngularFireAuthGuard],
        loadChildren: () =>
          import("../infohub/infoticket/tickets.module").then(
            (m) => m.TicketsPageModule
          ),
      },
      {
        path: "categories/userSettings",
        canActivate: [AngularFireAuthGuard],
        loadChildren: () =>
          import("../sett/settings.module").then((m) => m.SettingsPageModule),
      },
      {
        path: "infohub/info/:slug",
        loadChildren: () =>
          import("../infohub/categories/info/info.module").then(
            (m) => m.InfoPageModule
          ),
      },
      {
        path: "categories",
        loadChildren: () =>
          import("../infohub/categories/categories.module").then(
            (m) => m.CategoriesPageModule
          ),
      },
      {
        path: "infohub",
        loadChildren: () =>
          import("../infohub/infohub.module").then((m) => m.InfohubPageModule),
      },
      {
        path: "",
        redirectTo: "/tabs/tab1",
        pathMatch: "full",
      },
      {
        path: "chat/:uid/:username/:avatar",
        loadChildren: () =>
          import("../chat/chat.module").then((m) => m.ChatPageModule),
      },
      {
        path: "bulletin-board",
        loadChildren: () =>
          import("../bulletin-board/bulletin-board.module").then(
            (m) => m.BulletinBoardPageModule
          ),
      }
    ],
  },

  {
    path: "",
    redirectTo: "/tabs/tab1",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
