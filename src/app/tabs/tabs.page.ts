import { Component } from "@angular/core";
import { Storage } from "@ionic/storage";
import { BadgeService } from "../services/badge.service";
import { ChatService } from "../services/chat.service";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"],
})
export class TabsPage {
  notificationBadge: boolean;
  newPostNotificationBadge: boolean = true;
  newMessageBadge: boolean;
  constructor(
    private badgeService: BadgeService,
    private storage: Storage,
    private chatService: ChatService
  ) {}
  async ngOnInit() {
    this.storage.get("favoriteAdded").then((res) => {
      this.notificationBadge = res;
    });
    this.storage.get("newMessage").then((res) => {
      this.newMessageBadge = res;
    });
    this.badgeService.getFavoriteBadge().subscribe((res) => {
      this.notificationBadge = res;
    });
    this.badgeService.getMessageBadge().subscribe((res) => {
      this.newMessageBadge = res;
    });
    this.badgeService.getNewPostBadge().subscribe((res) => {
      this.newPostNotificationBadge = res;
    });
  }
  removeFavoriteBadge() {
    this.badgeService.updateFavoriteBadge(false);
  }
  removeMessageBadge() {
    this.badgeService.updateMessageBadge(false);
  }
}
