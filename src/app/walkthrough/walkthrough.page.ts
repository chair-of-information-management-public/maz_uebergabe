import { isPlatformBrowser } from "@angular/common";
import {
  Component,
  AfterViewInit,
  ViewChild,
  HostBinding,
  PLATFORM_ID,
  Inject,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import {
  AlertController,
  IonSlides,
  MenuController,
  ToastController,
} from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { LanguageService } from "../language/language.service";
import { Environment } from "../services/environment.service";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-walkthrough",
  templateUrl: "./walkthrough.page.html",
  styleUrls: [
    "./styles/walkthrough.page.scss",
    "./styles/walkthrough.shell.scss",
    "./styles/walkthrough.responsive.scss",
  ],
})
export class WalkthroughPage implements AfterViewInit {
  slidesOptions: any = {
    zoom: {
      toggle: false, // Disable zooming to prevent weird double tap zomming on slide images
    },
  };

  @ViewChild(IonSlides, { static: true }) slides: IonSlides;

  @HostBinding("class.first-slide-active") isFirstSlide = true;

  @HostBinding("class.last-slide-active") isLastSlide = false;
  subscriptions: Subscription;
  translations;
  flag;
  name;
  obj;
  available_languages = [];

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    public menu: MenuController,
    public translate: TranslateService,
    public languageService: LanguageService,
    public route: ActivatedRoute,
    public alertController: AlertController,
    private storage: Storage,
    public menuCtrl: MenuController,
    public router: Router
  ) {}
  ionViewWillEnter() {
    this.obj = this.languageService.getFlags(this.translate.currentLang);

    this.flag = this.obj.flag;
    this.name = this.obj.name;
    this.subscriptions = this.route.data.subscribe(
      (state) => {
        this.getTranslations();
      },
      (error) => console.log(error)
    );

    this.translate.onLangChange.subscribe(() => this.getTranslations());
  }
  // Disable side menu for this page
  ionViewDidEnter(): void {
    this.menu.enable(false);
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }

  ngAfterViewInit(): void {
    // Accessing slides in server platform throw errors
    if (isPlatformBrowser(this.platformId)) {
      // ViewChild is set
      this.slides.isBeginning().then((isBeginning) => {
        this.isFirstSlide = isBeginning;
      });
      this.slides.isEnd().then((isEnd) => {
        this.isLastSlide = isEnd;
      });

      // Subscribe to changes
      this.slides.ionSlideWillChange.subscribe((changes) => {
        this.slides.isBeginning().then((isBeginning) => {
          this.isFirstSlide = isBeginning;
        });
        this.slides.isEnd().then((isEnd) => {
          this.isLastSlide = isEnd;
        });
      });
    }
  }
  async openLanguageChooser() {
    this.available_languages = this.languageService
      .getLanguages()
      .map((item) => ({
        name: item.name,
        type: "radio",
        label: item.name,
        value: item.code,
        flag: item.flag,
        checked: item.code === this.translate.currentLang,
      }));

    const alert = await this.alertController.create({
      header: this.translations.SELECT_LANGUAGE,
      inputs: this.available_languages,
      cssClass: "language-alert",
      buttons: [
        {
          text: this.translations.CANCEL,
          role: "cancel",
          cssClass: "secondary",
          handler: () => {},
        },
        {
          text: this.translations.OK,
          handler: async (data) => {
            if (data) {
              await this.translate.use(data);
              await this.storage.set("language", data);
              this.translate.currentLang = await data;
              //this.changeGoogleMapsLanguage(data);
            }
            this.obj = await this.languageService.getFlags(
              this.translate.currentLang
            );
            this.flag = this.obj.flag;
            this.name = this.flag.name;
            window.location.reload(); //reload app after language change
          },
        },
      ],
    });
    await alert.present();

    var radios = document.getElementsByClassName("alert-radio-label");
    for (let index = 0; index < radios.length; index++) {
      let singrad = radios[index];
      singrad.innerHTML = singrad.innerHTML.concat(
        "<img src=" +
          this.available_languages[index].flag +
          ' style="width:30px; position:absolute; right:10px;"/>'
      );
    }
  }

  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(this.translate.currentLang)
      .subscribe((translations) => (this.translations = translations));
  }
  skipWalkthrough(): void {
    // Skip to the last slide
    this.slides.length().then((length) => {
      this.slides.slideTo(length);
    });
  }
}
